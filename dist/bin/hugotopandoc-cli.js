#!/usr/bin/env node
"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Node Package Modules
 */
const process_1 = require("process");
/**
 * Local Library Modules
 */
const lib_1 = require("../lib");
//
// START CLI Script
//
lib_1.convertHugoBookContentToPanDocMd(process_1.argv[2], process_1.argv[3], process_1.argv[4]);
