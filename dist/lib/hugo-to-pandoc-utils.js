"use strict";
/**
 * HUGO SSG Utils in TypeScript
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.preProcessHugoBookContentToPanDocMd = exports.convertHugoBookContentToPanDocMd = void 0;
// NodeJs system package imports
const readdir_1 = require("readdir");
const path_1 = require("path");
const fs_1 = require("fs");
//import { inspect } from 'util';
// Additional ecosystem package imports
const fs_extra_1 = require("fs-extra");
const front_matter_1 = __importDefault(require("front-matter"));
// Local imports 
const hugo_to_pandoc_config_1 = require("./hugo-to-pandoc-config");
/**
 * Export HUGO Book Theme based Content to PanDoc (input ready) Markdown
 * and create PanDoc Defaults File to execute Latex PDF Engine Document
 *
 * @param contentDir HUGO Book Theme content directory
 * @param outputFIle PanDoc LatEX PDF output file location
 * @param exportDir PanDoc pre-processed input directory
 */
function convertHugoBookContentToPanDocMd(contentDir = 'content', outputFile = 'exports/site.pdf', exportDir = 'pandoc-content') {
    // Prepare HUGO Book content for PanDoc LateX PDF Engine Markdown and creat PanDoc Defaults YAML:
    // - Parsing PanDoc input files with pre-processed files
    // - Parsing PanDoc resource files with local HUGO SSG resources (figures, documents, data, etc.) 
    const prepDataLists = preProcessHugoBookContentToPanDocMd(contentDir, exportDir);
    createHugoBookPanDocDefaultsFile(contentDir, outputFile, exportDir, prepDataLists.contentFileList, prepDataLists.resourcePathList);
}
exports.convertHugoBookContentToPanDocMd = convertHugoBookContentToPanDocMd;
/**
 * Creates a PanDoc Defaults file to process R Markdown prepared HUGO Markdown content
 *
 * @param contentDir HUGO content directory path
 * @param exportDir PanDoc prepared export ditectory path
 * @param contentFileList PanDoc R Markdown prepared "input-files"
 * @param resourcePathList PanDoc prepared "resource-path" items
 */
function createHugoBookPanDocDefaultsFile(contentDir, outputFile, exportDir, contentFileList, resourcePathList) {
    let inputFiles = '';
    for (const contentFile of contentFileList) {
        inputFiles += `- ${path_1.join(exportDir, contentDir, contentFile)}\n`;
    }
    let resourcePaths = '';
    for (const resourcePath of resourcePathList) {
        resourcePaths += `- ${path_1.join(exportDir, resourcePath)}\n`;
    }
    // Parse input file and resource paths list into PanDoc Defaults Template
    const panDocDefaultsFile = fs_1.readFileSync(hugo_to_pandoc_config_1.panDocDefaultsTemplateFilePath, 'utf-8')
        .replace('<%= inputFiles %>', inputFiles)
        .replace('<%= resourcePaths %>', resourcePaths)
        .replace('<%= panDocLatexTemplateFilePath %>', hugo_to_pandoc_config_1.panDocLatexTemplateFilePath)
        .replace('<%= outputFile %>', outputFile);
    // Write PanDoc HUGO Book Defaults File
    fs_extra_1.outputFileSync(path_1.join(exportDir, hugo_to_pandoc_config_1.hugoBookDefaultsFile), panDocDefaultsFile);
}
/**
 * Prepare HUGO Content Management based Markdown content for PanDoc Md processing
 * and create ordered input file content list and resource paths list
 *
 * @param contentDir directory with HUGO Markdown-content input
 * @param exportDir directory with PanDoc LateX prepared Markdown-content output
 * @returns ordered input content list for PanDoc processing and resource paths (directies with non Markdown-files)
 */
function preProcessHugoBookContentToPanDocMd(contentDir, exportDir) {
    const contentDirList = [];
    const resourcePathList = [];
    const options = [readdir_1.ReadDirOptions.CASELESS_SORT];
    const filePaths = readdir_1.readSync(contentDir, ['**.*'], options);
    // Prepare HUGO based Content for PanDoc input and
    // create a content list to process by PanDoc
    let dirWeight = 0;
    for (const filePath of filePaths) {
        // extract directory level and path data
        const filePathList = filePath.split(path_1.sep);
        const pathDepth = filePathList.length - 1;
        const fileName = filePathList[pathDepth];
        // content/< headingLevel = 1 >/docs/< headingLevel = 2 >/< headingLevel = 3 >/.../< headingLevel = (n) >
        const dirPath = filePathList.slice(0, pathDepth).join(path_1.sep);
        // build input and output file paths
        const inputFilePath = path_1.join(contentDir, filePath);
        const exportFilePath = path_1.join(exportDir, contentDir, dirPath, fileName);
        if (['.md', 'markdown'].find(fileExtension => filePath.endsWith(fileExtension))) {
            // pre-process HUGO Markdown format to PanDoc R Markdown
            // get Markdown Front Matter attributes
            const mdData = front_matter_1.default(fs_1.readFileSync(inputFilePath, 'utf-8'));
            // get directory weight within directory level
            const weight = mdData.attributes.weight ? mdData.attributes.weight : 0;
            dirWeight = fileName == '_index.md' ? weight : dirWeight;
            // get file weight within directory
            const fileWeight = fileName == '_index.md' ? 0 : weight;
            // Write PanDoc Md Pre-Processesed output file
            fs_extra_1.outputFileSync(exportFilePath, preProcessHugoBookDataToPanDocMd(mdData, pathDepth));
            // Add Markdown file location/description to HUGO Dir Content List structure
            addContentDirItem(contentDirList, {
                weight: dirWeight,
                path: dirPath,
                fileList: [{
                        weight: fileWeight,
                        name: fileName
                    }]
            });
        }
        else {
            // pre-process local resource file
            // create export directory and copy resource file
            fs_extra_1.ensureFileSync(exportFilePath);
            fs_extra_1.copyFileSync(inputFilePath, exportFilePath);
            const currentResourcePath = path_1.join(contentDir, dirPath);
            if (!resourcePathList.find(resourcePath => resourcePath == currentResourcePath)) {
                // Add new resource directory to resource path list
                resourcePathList.push(currentResourcePath);
            }
        }
    }
    // write HUGO order structure debug file
    fs_extra_1.outputFileSync(path_1.join(exportDir, 'contentDirList.JSON'), JSON.stringify(contentDirList));
    return { contentFileList: createContentList(contentDirList), resourcePathList };
}
exports.preProcessHugoBookContentToPanDocMd = preProcessHugoBookContentToPanDocMd;
/**
 *
 * @param dirList
 * @param contentList
 * @returns
 */
function createContentList(dirList, contentList = []) {
    // add directories on this level
    for (const dirItem of dirList) {
        if (dirItem.fileList) {
            // files found for this directory level
            // add files on this directory level
            for (const fileItem of dirItem.fileList) {
                contentList.push(path_1.join(dirItem.path, fileItem.name));
            }
        }
        if (dirItem.dirList) {
            // sub directories found for this directory level
            // add sub directories on this directory level 
            contentList = createContentList(dirItem.dirList, contentList);
        }
    }
    return contentList;
}
/**
 * Add HUGO Content Directory Item to list with recursive structure according
 * [HUGO Book Theme - File tree menu structure](https://github.com/alex-shpak/hugo-book#file-tree-menu-default)
 *
 * @param dirList HUGO Content Directory list to add new items
 * @param newDirItem new HUGO Content Directory item to add
 *
 */
function addContentDirItem(dirList, newDirItem) {
    // search for matching (sub) dir item
    // and add new file item when exists
    for (const dirItem of dirList) {
        if (dirItem.path == newDirItem.path) {
            // exact dir path match!
            if (dirItem.fileList && newDirItem.fileList) {
                // add new file item
                dirItem.fileList.push(newDirItem.fileList[0]);
                // apply HUGO File tree menu order
                dirItem.fileList.sort((a, b) => a.weight - b.weight);
            }
            else {
                // file list not exixts
                // initialize file list
                dirItem.fileList = newDirItem.fileList;
            }
            return;
        }
        else if (newDirItem.path.startsWith(dirItem.path)) {
            // partial dir path match!
            if (!dirItem.dirList) {
                // sub dir list not exixts
                // initialize sub dir list
                dirItem.dirList = [];
            }
            // add new dir item
            addContentDirItem(dirItem.dirList, newDirItem);
            // apply HUGO File tree menu order
            dirItem.dirList.sort((a, b) => a.weight - b.weight);
            return;
        }
    }
    // no dir item found!
    // add initial dir item
    dirList.push(newDirItem);
    return;
}
/**
 * Pre-process Hugo Book Front Matter Data to PanDoc Markdown requirements
 *
 * @param mdData Markdown File Front Matter Data (see https://github.com/jxson/front-matter)
 * @param pathDepth HUGO Book directory path depth level
 *
 * @returns PanDoc required Markdown Latex PDF-engine Formatted string
 */
function preProcessHugoBookDataToPanDocMd(mdData, pathDepth) {
    // create list of applied HUGO ShortCodes
    // using basic match pattern {{< ShortCode (parms) >}}
    const shortCodeList = mdData.body.match(/{{\<.*?\>}}/g);
    if (shortCodeList == null) {
        // No HUGO ShortCodes found
        // So nothing to parse
        return mdData.body;
    }
    // initialize applied ShortCode index
    let shortCodeIndex = 0;
    // initialize empty pre-processed output list
    let mdOutputList = [];
    // create Markdown output lines with ShortCodes parsed in Latex-formatted lines
    for (const mdInputItem of mdData.body.split('\n')) {
        //console.log(`hortCodeList[${shortCodeIndex}] = ${shortCodeList[shortCodeIndex]}`);
        //console.log(`mdInputItem = ${mdInputItem}`);
        // initialize Markdown output line with input line contents (default)
        let mdOutputItem = mdInputItem;
        while (shortCodeIndex < shortCodeList.length && mdOutputItem.includes(shortCodeList[shortCodeIndex])) {
            // line item contains current applied ShortCode
            // parse Latex parsed Markdown to output line
            //console.log(`PRE: ${mdOutputItem}`);
            mdOutputItem = parseHugoshortCodeToPanDocMd(shortCodeList[shortCodeIndex], mdOutputItem, mdData.attributes);
            //console.log(`POST: ${mdOutputItem}`);
            // move to next applied ShortCode
            shortCodeIndex++;
        }
        if (mdOutputItem.startsWith('#')) {
            // Markdown Heading line found!
            // pre-process Heading level based on directory depth level
            if (pathDepth > 1) {
                // Add Sub Paragraph Heading ident
                mdOutputItem = '#'.repeat(pathDepth - 2) + mdOutputItem;
            }
        }
        // add pre-processed Markdown output line
        mdOutputList.push(mdOutputItem);
    }
    // return pre-processed Markdown string
    return mdOutputList.join('\n');
}
/**
 * Replace HUGO ShortCode Markdown input PanDoc R Markdown (or remove code)
 *
 * See https://rmarkdown.rstudio.com/authoring_pandoc_markdown.html#Pandoc_Markdown
 *
 * @param shortCodeItem Original HUGO ShortCode Definition
 * @param mdInputItem Markdown input with Original HUGO ShortCode Definition
 * @returns PanDoc Markdown R wirh mapped (LaTeX) statement (or HTML-remark of removal)
 */
function parseHugoshortCodeToPanDocMd(shortCodeItem, mdInputItem, attributes) {
    let mdOutputItem = '';
    // get tag [0] (and parms [1..n])
    const shortCodeBody = shortCodeItem.split('{{<')[1].split('>}}')[0].trim().split(' ');
    const shortCodeTag = shortCodeBody[0].trim();
    const panDocMapping = hugo_to_pandoc_config_1.hugoShortCodeToPanDocMapping[shortCodeTag];
    if (panDocMapping) {
        // HUGO ShortCode to PanDoc Mapping found
        // Apply Tag Mapping
        mdOutputItem = mdInputItem.replace(shortCodeItem, panDocMapping);
        if (shortCodeBody.length > 1) {
            // HUGO ShortCode Parameters found
            // Apply Parameter Mapping
            let i = 0;
            for (const parm of shortCodeBody.slice(1)) {
                // parse next parm value
                i++;
                mdOutputItem = mdOutputItem.replace(`\<=\% parm${i} \%\>`, parm);
                //mdOutputItem = mdOutputItem.replace(`\<=\% parm${i} \%\>`, parmValue);
            }
        }
    }
    else {
        // HUGO ShortCode to PanDoc Mapping NOT found
        // Handle excecptions that can not be parsed directly
        switch (shortCodeTag) {
            case 'param':
                // parse Front Matter attribute as parameter as Text
                let paramValue = '';
                switch (shortCodeBody[1]) {
                    case 'title':
                        if (attributes.title) {
                            paramValue = attributes.title;
                        }
                        break;
                    case 'description':
                        if (attributes.description) {
                            paramValue = attributes.description;
                        }
                        break;
                    case 'weight':
                        if (attributes.weight) {
                            paramValue = String(attributes.weight);
                        }
                        break;
                }
                if (paramValue != '') {
                    mdOutputItem = mdInputItem.replace(shortCodeItem, paramValue);
                }
                else {
                    console.warn(`Front Mattter Attribute ${shortCodeBody[1]} Not Found!`);
                }
                break;
            default:
                // Remove unsed code
                mdOutputItem = mdInputItem.replace(shortCodeItem, `\n<!-- Removed HUGO ShortCode: ${shortCodeItem} -->\n`);
                break;
        }
    }
    return mdOutputItem;
}
