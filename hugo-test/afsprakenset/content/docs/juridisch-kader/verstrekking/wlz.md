---
title: "WLZ"
bookToc: false
description: "Wet langdurige zorg"
weight: 30
---

# **[{{< param description >}}](https://wetten.overheid.nl/BWBR0035917/2019-11-27)**

Geldend van 27-11-2019 t/m heden

{{< columns >}}

**Relevante artikelen**

*Artikel 9.1.5*

1. Het Zorginstituut en de zorgautoriteit verstrekken desgevraagd aan Onze Minister of aan het College sanering, genoemd in [artikel 32 van de Wet toelating zorginstellingen](https://wetten.overheid.nl/jci1.3:c:BWBR0018906&artikel=32&g=2019-12-05&z=2019-12-05), de voor de uitoefening van hun taak benodigde inlichtingen en gegevens.
2. Het Zorginstituut en de zorgautoriteit verlenen aan door Onze Minister of door het College sanering aangewezen personen toegang tot en inzage in zakelijke gegevens en bescheiden, voor zover dat voor de vervulling van hun taak redelijkerwijs nodig is.

*Artikel 10.4.1*

1. De ambtenaren van de Inspectie gezondheidszorg en jeugd zijn belast met het toezicht op de naleving door zorgaanbieders van de verplichtingen die voor hen uit het bepaalde bij of krachtens [hoofdstuk 8](https://wetten.overheid.nl/BWBR0035917/2019-11-27#Hoofdstuk8) voortvloeien.
2. De in het eerste lid bedoelde ambtenaren zijn, voor zover dat voor de vervulling van hun taak noodzakelijk is, bevoegd tot inzage van de dossiers van verzekerden. In afwijking van [artikel 5:20, tweede lid, van de Algemene wet bestuursrecht](https://wetten.overheid.nl/jci1.3:c:BWBR0005537&artikel=5:20&g=2019-12-05&z=2019-12-05), dienen ook zorgverleners die uit hoofde van hun beroep tot geheimhouding van de dossiers verplicht zijn, de ambtenaren, bedoeld in de eerste volzin, inzage te geven in de daar bedoelde dossiers. In dat geval zijn de betrokken ambtenaren verplicht tot geheimhouding van de dossiers.

<--->

**Toepassing voor de afspraken**

*Artikel 9.1.5* is een artikel dat gaat over doorlevering en dat stelt dat de NZa en het Zorginstituut gegevens verstrekken aan de Minister voor de uitoefening van de taak. De formulering van ‘hun taak’ is dusdanig ruim dat ook taken buiten deze wet hieronder zouden kunnen vallen. Het is de vraag of dit zo bedoeld is.
  

*Artikel 10.4.1* stelt dat de IGJ toezicht moet houden op de invulling van de randvoorwaarden door zorgaanbieders zoals gesteld in hoofdstuk 8 van de Wlz. De randvoorwaarden in hoofdstuk 8 gaan over de afspraken die de zorgaanbieder maakt met de cliënt over goede zorg. Daarbij mogen ze dossiers van verzekerden inzien.
  

Afgezien van *artikel 9.1.5* en *artikel 10.4.1* heeft de wet Wlz met name betrekking over de relatie tussen Wlz-uitvoerders en andere systeempartijen zoals Zorginstituut en de Zorgautoriteit. Deze wet is daarom minder relevant voor KIK-V.

{{< /columns >}}