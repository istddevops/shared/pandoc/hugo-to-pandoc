---
title: "BEGDZA"
bookToc: false
description: "Besluit elektronische gegevensverwerking door zorgaanbieders"
weight: 20
---

# **[{{< param description >}}](https://wetten.overheid.nl/BWBR0040238/2018-12-15)**

Geldend van 15-12-2018 t/m heden

{{< columns >}}

**Relevante artikelen**

*Artikel 3*
  
1. De verantwoordelijke voor een elektronisch uitwisselingssysteem draagt overeenkomstig het bepaalde in NEN 7510 en NEN 7512, zorg voor een veilig en zorgvuldig gebruik van dat elektronisch uitwisselingssysteem.
2. Een zorgaanbieder draagt overeenkomstig het bepaalde in NEN 7510 en NEN 7512, zorg voor een veilig en zorgvuldig gebruik van het zorginformatiesysteem en een veilig en zorgvuldig gebruik van het elektronisch uitwisselingssysteem waarop hij is aangesloten.
3. De verantwoordelijke voor een elektronisch uitwisselingssysteem werkt met een zorgserviceprovider die is geautoriseerd op basis van overeenkomstig NEN 7512 vastgestelde criteria.
4. Een rechtspersoon, niet zijnde een zorgaanbieder, die een elektronisch uitwisselingssysteem beheert en in stand houdt, voldoet aan de volgende voorwaarden:
    - a. een van de rechtspersoon onafhankelijke organisatie heeft na onderzoek vastgesteld dat de rechtspersoon en het systeem dat hij beheert voldoen aan het bepaalde in NEN 7510 en NEN 7512 en heeft die bevinding opgenomen in een door die organisatie ten behoeve van de rechtspersoon opgesteld audit-rapport;
    - b. de in onderdeel a bedoelde vaststelling is niet langer dan vijf jaar geleden gedaan.

*Artikel 5*
  
1. De zorgaanbieder als verantwoordelijke voor een zorginformatiesysteem en de verantwoordelijke voor een elektronisch uitwisselingssysteem dragen er zorg voor dat de logging van het systeem voldoet aan het bepaalde in NEN 7513.
2. Vertegenwoordigende organisaties van zorgaanbieders en patiënten stellen, in overleg met het College bescherming persoonsgegevens, overeenkomstig het bepaalde in NEN 7513 de bewaartermijn voor logging vast en maken die bewaartermijn bekend in de Staatscourant binnen 6 maanden na de inwerkingtreding van dit besluit. Indien niet binnen deze termijn een bewaartermijn bekend is gemaakt, stelt Onze Minister van Volksgezondheid, Welzijn en Sport een bewaartermijn vast en maakt die bekend in de Staatscourant.

<--->

**Toepassing voor de afspraken**

Van de bevoegdheid volgend uit *artikel 15j in de wet aanvullende bepalingen verwerking persoonsgegevens in de zorg* is gebruik gemaakt in het Besluit elektronische gegevensverwerking door zorgaanbieders, waarin in art. 3 de NEN-normen NEN 7510 en 7512 verplicht zijn gesteld voor zorgaanbieders en waarin in art. 5 de NEN-norm NEN 7513 verplicht is gesteld voor logging. Hieruit vloeit op voor de medewerkers van zorgaanbieders kenbare en voorzienbare wijze uit voort dat er logging plaatsvindt van hun gebruikershandelingen in zorginformatiesystemen en elektronische uitwisselingssystemen.

{{< /columns >}}