---
title: "WOR"
bookToc: false
description: "Wet op de ondernemingsraden"
weight: 10
---

# **[{{< param description >}}](https://wetten.overheid.nl/BWBR0002747/2020-01-01)**
  
Geldend van 01-01-2020 t/m heden

{{< columns >}}

**Relevante artikelen**

[Artikel 27](https://wetten.overheid.nl/jci1.3:c:BWBR0002747&hoofdstuk=IVA&artikel=27&z=2020-01-01&g=2020-01-01
)
  

1. De ondernemer behoeft de instemming van de ondernemingsraad voor elk door hem voorgenomen besluit tot vaststelling, wijziging of intrekking van:<br>(a t/m j)<br>k.	een regeling omtrent het verwerken van alsmede de bescherming van de persoonsgegevens van de in de onderneming werkzame personen;
  

*(l en m)*
een en ander voor zover betrekking hebbende op alle of een groep van de in de onderneming werkzame personen.
  

2. De ondernemer legt het te nemen besluit schriftelijk aan de ondernemingsraad voor. Hij verstrekt daarbij een overzicht van de beweegredenen voor het besluit, alsmede van de gevolgen die het besluit naar te verwachten valt voor de in de onderneming werkzame personen zal hebben. De ondernemingsraad beslist niet dan nadat over de betrokken aangelegenheid ten minste éénmaal overleg is gepleegd in een overlegvergadering. Na het overleg deelt de ondernemingsraad zo spoedig mogelijk schriftelijk en met redenen omkleed zijn beslissing aan de ondernemer mee. Na de beslissing van de ondernemingsraad deelt de ondernemer zo spoedig mogelijk schriftelijk aan de ondernemingsraad mee welk besluit hij heeft genomen en met ingang van welke datum hij dat besluit zal uitvoeren.
  

*(lid 3 t/m 7)*

<--->

**Toepassing voor de afspraken**

Aanbieders verwerken in het kader van KIK-V persoonsgegevens van eigen medewekers voor de aanbiedergegevensset en voor logging. Afnemers verwerken persoonsgegevens van eigen medewerkers alleen voor logging. Artikel 27 lid 1 sub k van de Wet op de ondernemingsraden is daarom relevant. Deze schrijft instemming door de ondernemingsraad van de organisatie voor bij iedere regeling ten aanzien van verwerking van persoonsgegevens van in de onderneming werkzame personen. Alle afnemers en aanbieders (met een ondernemingsraad) zullen hierover in overleg moeten treden met hun ondernemingsraden, voor zover zij dit niet al hebben gedaan in het kader van hun algemene informatiebeveiligingsbeleid.

{{< /columns >}}