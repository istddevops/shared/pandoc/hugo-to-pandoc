---
title: "Beheerafspraken"
bookCollapseSection: true
weight: 10
---

# Beheerafspraken

{{< hint info >}}
Ontwikkelingen binnen en rondom KIK-V kunnen aanleiding geven om de afsprakenset aan te passen. Zo kunnen er bijvoorbeeld nieuwe informatievragen ontstaan of kunnen bestaande informatievragen minder relevant worden. Actief onderhoud is nodig om de afspraken samenhangend, actueel en relevant te houden. De afspraken kennen een dynamisch karakter.
{{< /hint >}}

## Doelen

De beheerafspraken **dragen bij aan transparantie** van de afsprakenset. Processen, rollen van de betrokken partijen en afwegingen worden hierdoor navolgbaar. Deze transparantie draagt bij aan het vertrouwen in (de totstandkoming van) de afsprakenset.
  

Naast de bijdrage aan transparantie, pogen de beheerafspraken ook de volgende twee doelen met elkaar in balans te brengen:

- **Bijdragen aan standaardisatie**: De beheerafspraken dienen bij te dragen aan het standaardiserend effect van KIK-V. Standaardisatie is voor zowel aanbieders (o.a. minder administratieve lasten) als afnemers (o.a. betere kwaliteit van gegevens) van meerwaarde;
- **Bieden van voldoende flexibiliteit**: Het is voor afnemers niet altijd mogelijk om van te voren te voorspellen welke informatie zij nodig hebben. De beheerprocessen zullen hier rekening mee moeten houden en een zekere mate van flexibiliteit mogelijk moeten maken, wil de afsprakenset kunnen rekenen op voldoende draagvlak onder afnemers. Voor aanbieders is flexibiliteit minder van belang.
Om beide doelen met elkaar in balans te brengen en tot een gebalanceerde beheeroplossing voor de afsprakenset te komen, wordt de ontwikkeling en vaststelling van uitwisselprofielen losgetrokken van de ontwikkeling en vaststelling van de rest van de afsprakenset. Uitwisselprofielen die voldoen aan de kaders van de afsprakenset, kunnen daardoor buiten de gecontroleerde en relatief “zware” releasecyclus van de afsprakenset tot stand komen.
  

Deze beheeroplossing is vertaald naar een beheerproces. Dit proces wordt door de beheerorganisatie periodiek herijkt, waarbij onder andere getoetst wordt in hoeverre de afspraken bijdragen aan de te realiseren doelen.

## Proces

De beheerafspraken zijn opgebouwd aan de hand van de volgende processtappen:

- **Verkennen**: Een externe ontwikkeling heeft mogelijk impact op de afsprakenset en/of uitwisselprofielen. Verkennen beschrijft hoe een signaal over zo’n externe ontwikkeling binnenkomt, wordt verwerkt en hoe wordt bepaald welke actie noodzakelijk is.
- **Ontwikkelen**: De externe ontwikkeling vraagt om (mogelijke) wijziging(en) aan de afsprakenset en/of uitwisselprofielen. Ontwikkelen beschrijft hoe dit ontwikkelproces vormkrijgt.
- **Vaststellen**: Indien ontwikkelen resulteert in een wijziging aan de afsprakenset en/of uitwisselprofielen, dan zal deze wijziging moeten worden vastgesteld. Vaststellen beschrijft de relevante afspraken over het vaststellen van de afsprakenset en uitwisselprofielen.
- **Publiceren**: Nieuwe versies van de afsprakenset en/of uitwisselprofielen dienen te worden gepubliceerd, alvorens ze kunnen worden geïmplementeerd. Publiceren beschrijft de afspraken die daarop van toepassing zijn.
- **Implementeren**: Wijziging(en) worden vervolgens geïmplementeerd. Implementeren beschrijft de afspraken die daarvoor gelden.
- **Gebruiken en evalueren**: De publicatie van de afsprakenset en/of uitwisselprofielen zal in praktijk worden toegepast en nieuwe wensen opleveren voor doorontwikkeling. Gebruiken en evalueren beschrijft de afspraken die daarvoor gelden. Deze zijn tevens van toepassing op de beheerafspraken zelf.

![Beheerafspraken](beheerafspraken.png)

### 1. Verkennen

Signalen over externe ontwikkelingen komen binnen bij en/of worden gedeeld met de beheerorganisatie. Er kan ondermeer andere sprake zijn van de volgende situaties:

- Een afnemer heeft een nieuwe of gewijzigde informatiebehoefte;
- Een deelnemer komt met een verzoek tot het wijzigen van een van de afspraken;
- Een externe standaard, die in de afsprakenset wordt toegepast, gaat wijzigen/is gewijzigd.

De beheerorganisatie is verantwoordelijk om te bepalen of er wijzigingen aan de afsprakenset en/of uitwisselprofielen nodig zijn. De eerste stap daarbij is om te bepalen of de externe ontwikkeling impact heeft op de afsprakenset en/of uitwisselprofielen en met welke urgentie het signaal moet worden opgepakt.

Om te bepalen op welke manier een externe ontwikkeling impact heeft, hanteert de beheerorganisatie de volgende keuzeboom:

1. Is de externe ontwikkeling een (gewijzigde) informatiebehoefte van een afnemer?
   - a. Zo ja, dan heeft de wijziging in ieder geval betrekking op de uitwisselprofielen. Bij [Ontwikkeling uitwisselprofielen](ontwikkeling/uitwisselprofielen) staat nader beschreven hoe de (door)ontwikkeling van uitwisselprofielen plaatsvindt. Ga verder naar vraag 2.
   - b. Zo nee, ga verder naar vraag 6.
2. Is er voor de (gewijzigde) informatiebehoefte sprake van een wettelijke grondslag of zijn er andere juridische standaardafspraken waarbij het uitwisselprofiel (kan) aansluit(en)?
   - a. Zo ja, ga verder naar vraag 3;
   - b. Zo nee, dan is er naast de (door)ontwikkeling van een uitwisselprofiel sprake van een wijziging aan de afsprakenset. Bij [Ontwikkeling afsprakenset](ontwikkeling/afsprakenset) staat nader beschreven hoe de (door)ontwikkeling van de afsprakenset plaatsvindt. Ga verder naar vraag 3.
3. Kan de (gewijzigde) informatiebehoefte bij het uitwisselprofiel worden beantwoord aan de hand van de modelgegevensset?
   - a. Zo ja, ga verder naar vraag 4;
   - b. Zo nee, dan is er naast de (door)ontwikkeling van een uitwisselprofiel sprake van een wijziging aan de afsprakenset. Bij [Ontwikkeling afsprakenset](ontwikkeling/afsprakenset) staat nader beschreven hoe de (door)ontwikkeling van de afsprakenset plaatsvindt. Ga verder naar vraag 4;
4. Sluit het gewenste moment/sluiten de gewenste momenten van gegevensuitwisseling aan bij de overeengekomen uitwisselmomenten op de uitwisselkalender?
   - a. Zo ja, ga naar vraag 5;
   - b. Zo nee, dan is er naast de (door)ontwikkeling van een uitwisselprofiel sprake van een wijziging aan de afsprakenset. Bij [Ontwikkeling afsprakenset](ontwikkeling/afsprakenset) staat nader beschreven hoe de (door)ontwikkeling van de afsprakenset plaatsvindt. Ga verder naar vraag 5;
5. Is de gewenste technische wijze van gegevensuitwisseling een in de standaardafspraken overeengekomen werkwijze en ondersteunen de te bevragen aanbieders deze techniek?
   - a. Zo ja, dit was de laatste vraag.
   - b. Zo nee, dan is er naast de (door)ontwikkeling van een uitwisselprofiel sprake van een wijziging aan de afsprakenset. Bij [Ontwikkeling afsprakenset](ontwikkeling/afsprakenset) staat nader beschreven hoe de (door)ontwikkeling van de afsprakenset plaatsvindt. Dit was de laatste vraag.
6. Is de externe ontwikkeling een wijziging aan een in de afsprakenset toegepaste (externe) standaard, een direct verzoek tot wijziging van een van de afspraken of heeft de externe ontwikkeling op een andere manier invloed (op de opzet) van de afsprakenset?
   - a. Zo ja, dan is er sprake van een (mogelijke) wijziging aan de afsprakenset. Bij [Ontwikkeling afsprakenset](ontwikkeling/afsprakenset) staat nader beschreven hoe de (door)ontwikkeling van de afsprakenset plaatsvindt. Dit was de laatste vraag.
   - b. Zo nee, dit was de laatste vraag.

Aan de hand van de keuzeboom wordt bepaald of ontwikkeling van de afspraken aan de orde is én of “enkel” sprake is van een wijziging aan een uitwisselprofiel binnen de kaders van de afsprakenset of dat ook wijzigingen aan de afsprakenset nodig zijn. Indien (door)ontwikkeling niet aan de orde is, dan stopt het proces hier.

### 2. Ontwikkelen

Ondanks dat de ontwikkeling van de afsprakenset en de uitwisselprofielen uit elkaar is getrokken, hangt de ontwikkeling van de twee nauw met elkaar samen. Uitwisselprofielen dienen aan te sluiten bij de geldende versie van de afsprakenset. Indien dat niet het geval is, dan dient eerst de afsprakenset te wijzigen. Andersom geldt dat een wijziging van de afsprakenset invloed heeft op de uitwisselprofielen. Visueel ziet deze onderlinge afhankelijkheid bij de ontwikkeling er als volgt uit:

![Ontwikkelen](ontwikkelen.png)

De ontwikkeling van uitwisselprofielen is een proces tussen afnemer en uitvoerend orgaan en kent een “lichter” en flexibeler karakter dan de ontwikkeling van de afsprakenset. Zolang een uitwisselprofiel aansluit bij de afsprakenset, kan de ontwikkeling van het uitwisselprofiel hierdoor “lean and mean” blijven. De ontwikkeling van de afsprakenset is een proces tussen alle belanghebbenden en kent een voorspelbaarder karakter met een vaste ontwikkelcyclus. De afspraken die gelden voor de twee verschillende ontwikkelcycli staan nader beschreven bij [Ontwikkeling uitwisselprofielen](ontwikkeling/uitwisselprofielen) en [Ontwikkeling afsprakenset](ontwikkeling/afsprakenset).
  

Bij de ontwikkeling van de afsprakenset en de uitwisselprofielen is aandacht voor de onderlinge impact. Met de belanghebbende partijen worden, alvorens de nieuwe afspraken worden vastgesteld, afspraken gemaakt over het publiceren, in werking treden en implementeren van nieuwe versies van de afsprakenset en de uitwisselprofielen.

### 3. Vaststellen

De samenhang tussen de afsprakenset en de uitwisselprofielen heeft ook impact op het vaststellen. De volgende principes worden gehanteerd:

- De Ketenraad KIK-V is verantwoordelijk voor zowel het vaststellen van de afsprakenset als de uitwisselprofielen en is verantwoordelijk voor de samenhang tussen de producten.
- De Ketenraad KIK-V gaat bij het vaststellen van de afsprakenset na in hoeverre een gedegen proces is doorlopen en er bij de verschillende belanghebbenden draagvlak is voor de afspraken. Bij het vaststellen van (wijzigingen aan) de afsprakenset wordt altijd de impact op de uitwisselprofielen meegenomen, inclusief een voorstel voor de implementatie van de benodigde wijzigingen aan de uitwisselprofielen. Op die manier wordt geborgd dat uitwisselprofielen blijven aansluiten bij de afsprakenset.
- Het vaststellen van (wijzigingen aan) uitwisselprofielen kan zelfstandig plaatsvinden, zolang het betreffende uitwisselprofiel aansluit bij de afsprakenset. De Ketenraad KIK-V dient er bij het vaststellen op toe te zien dat dit inderdaad het geval is. Een uitwisselprofiel dat niet aansluit bij de afsprakenset kan pas worden vastgesteld, zodra de benodigde wijzigingen aan de afsprakenset door de Ketenraad KIK-V zijn vastgesteld.
- Om de impact op de bestuurlijke besluitvorming te beperken, kan het Tactisch overleg KIK-V onder mandaat van de Ketenraad KIK-V besluiten over kleine en/of bij spoedwijzigingen. Het Tactisch overleg KIK-V legt hierover verantwoording af aan de Ketenraad KIK-V. Bij deze besluitvorming dient rekening te worden gehouden met de hiervoor beschreven aandachtspunten over samenhang.
- Bij de besluitvorming hebben de Ketenraad KIK-V en het Tactisch overleg KIK-V een informatieachterstand ten opzichte van de beheerorganisatie. Om goed te kunnen besluiten en te bepalen in hoeverre er bij de besluitvorming risico’s worden gelopen, is het van belang dat de beheerorganisatie nieuwe releases voorziet van een advies. Het advies dient eventuele risico’s zichtbaar te maken door de volgende dingen uit te lichten:
    - In hoeverre er in de voorliggende release RFC’s zitten waarbij perspectieven van belanghebbende partijen sterk uiteen liepen, in hoeverre vervolgens tot consensus is gekomen en zo niet, welke positionering de beheerorganisatie voorstelt. Denk bijvoorbeeld aan RFC’s voor de modelgegevensset waarbij de belangen van aanbieders en afnemers uiteenlopen (voor dit voorbeeld geldt dat het Matchingsproces de relevante afwegingen beschrijft. De Ketenraad en het Tactisch overleg zullen met name op de hoogte moeten zijn van die afwegingen waarbij perspectieven uiteenlopen. Denk aan het al dan niet hergebruiken van een bestaande bron, het al dan niet toevoegen van gegevens aan de modelgegevensset, etc.).
    - In hoeverre er in de voorliggende release RFC’s zitten waarbij belangrijke belanghebbende partijen (om wat voor reden dan ook) niet betrokken zijn geweest;
    - In hoeverre er in de voorliggende release RFC’s zitten waarbij is afgeweken van het ontwikkelproces zoals beschreven in de beheerafspraken (denk aan het beperkt organiseren van toetsing door de mate van urgentie van een RFC).

### 4. Publiceren

Voor het publiceren en in werking treden van nieuwe versies van de afsprakenset en uitwisselprofielen gelden de volgende afspraken:

- De afsprakenset en uitwisselprofielen zijn publiek toegankelijk en hebben elk een eigen publicatieplaats.
- De afsprakenset en uitwisselprofielen kunnen in de tijd los van elkaar worden gepubliceerd.
- De uitwisselprofielen kunnen onderling in de tijd ook los van elkaar worden gepubliceerd.
- Publicatie van de afsprakenset en uitwisselprofielen kan, in lijn met gemaakte implementatieafspraken, op elk moment na vaststelling.
- Bij publicatie van de afsprakenset en uitwisselprofielen wordt vastgelegd wat het moment van inwerkingtreding is.
- Een publicatie van de afsprakenset kan, in lijn met gemaakte implementatieafspraken, op elk gewenst moment in werking treden. 
- Een publicatie van een uitwisselprofiel kan pas in werking treden zodra de daarvoor benodigde afsprakenset is gepubliceerd en in werking is getreden.
- In de regel kunnen er niet meer dan twee versies van de afsprakenset en van uitwisselprofielen naast elkaar actief zijn.
- Het Register van Uitwisselprofielen biedt een overzicht van de (historische en geldende) uitwisselprofielen, inclusief metadata over o.a. inwerkingtreding en geldigheid. 

Om onderscheid te kunnen maken tussen verschillende versies van de afsprakenset en de uitwisselprofielen, zijn de volgende afspraken over versionering nodig:

- De afsprakenset kent een eigen versionering volgens een navolgbare systematiek. 
- Elk uitwisselprofiel kent een eigen versionering volgens een navolgbare systematiek.
- Het Register van Uitwisselprofielen hoeft geen eigen versionering te kennen, zolang inzichtelijk is welke mutaties op welk moment hebben plaatsgevonden. Versionering is een van de mogelijkheden hiervoor.

### 5. Implementeren

De implementatie van een nieuwe versie van de afsprakenset vergt aanpassingen door vele partijen. Het is over het algemeen niet mogelijk om deze aanpassingen bij alle partijen tegelijkertijd door te voeren. Aan deelnemers wordt daarom bij de publicatie van nieuwe afspraken ruimte geboden om aan de afspraken te kunnen voldoen middels een implementatietermijn. Welke termijn toereikend is, wordt vastgelegd in de implementatieafspraken.
  

De implementatieafspraken worden, alvorens het vaststellen door de Ketenraad KIK-V, afgestemd door de beheerorganisatie met deelnemers en hun softwareleveranciers. Na vaststelling wordt van deelnemers verwacht dat zij zich houden aan de implementatiedata en zich vanaf de afgesproken momenten conformeren aan de (gewijzigde) afsprakenset. De beheerorganisatie ziet toe op de gecontroleerde implementatie van de afspraken.
  

Uitwisselprofielen die aansluiten bij de afsprakenset kennen in principe een beperkte implementatietermijn. Afnemers en aanbieders kunnen op basis van de bestaande afspraken en kaders aan het uitwisselprofiel voldoen.

### 6. Gebruiken en evalueren

De afsprakenset en uitwisselprofielen worden door de deelnemers in praktijk toegepast. Waar nodig ondersteunt de beheerorganisatie bij de interpretatie van de afspraken. Ook ziet zij toe op de naleving van de afspraken.
  

Het gebruik in praktijk levert nieuwe wensen op voor de doorontwikkeling. Deze wensen kunnen, in de vorm van 1) ge(her)formuleerde informatiebehoefte of 2) een wijzigingsverzoek voor de afsprakenset, worden ingediend bij het productmanagement van de afsprakenset. Het productmanagement zal de nieuwe wens behandelen volgens de hierboven beschreven stappen.

---
Zie ook
---

{{<section>}}
