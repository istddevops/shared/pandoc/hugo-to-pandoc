---
title: "Procesafspraken ODB"
description: "Procesafspraken stuurgroep kwaliteitskader voor uitwisselprofiel ODB."
weight: 20
---

# Procesafspraken uitwisselprofiel ODB

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

1. De stuurgroep betrekt KIK-V bij de werkzaamheden m.b.t. doorontwikkeling van de kwaliteits-uitvraag, zodat KIK-V op de hoogte is van de planning en inhoudelijke keuzes en zij de stuurgroep kan informeren over eventuele informatie-technische aspecten. Een vertegenwoordiger van KIK-V zal worden uitgenodigd plaats te nemen in de werkgroep indicatorenontwikkeling.
2. Informatie-technische aanpassingen m.b.t. de kwaliteits-uitvraag worden door KIK-V ingebracht in de werkgroep indicatorenontwikkeling voor inhoudelijke bespreking. Het gaat dan bijvoorbeeld om het inbrengen van ervaringen bij zorgaanbieders en softwareleveranciers ten aanzien van gegevens en berekeningen of zoals die voor andere uitvragen worden gebruikt.
3. Informatie-technische aanpassingen in KIK-V m.b.t. de kwaliteits-uitvraag kwaliteitskader worden –na bespreking in de werkgroep- ter besluitvorming voorgelegd aan de stuurgroep als onderdeel van het proces van indicatorenontwikkeling. Hiervoor zullen de ontwikkelagenda’s van beide partijen op elkaar worden afgestemd.
4. De stuurgroep en KIK-V informeren relevante samenwerkingspartners ieder voor zich over de deadlines in de ontwikkelagenda.
5. KIK-V informeert de stuurgroep periodiek over de stand van zaken van relevante producten en processen in de reguliere stuurgroepvergaderingen. Een vraagstuk of een verzoek voor eventuele aanpassing m.b.t. de kwaliteitsuitvraag door een deelnemer aan KIK-V wordt door het Zorginstituut ingebracht gedurende het jaar. De stuurgroep bepaalt waar het vraagstuk thuishoort en hoe deze verder op te pakken. Inhoudelijke uitwerking vindt plaats in de werkgroep indicatorenontwikkeling, waarvan KIK-V deel uitmaakt.
6. Overleg m.b.t. specifieke ontwikkelingen ‘registratie aan de bron’ vindt periodiek plaats tussen KIK-V en de beroepsgroepen. Inhoudelijke keuzes die hieruit voortkomen worden afgestemd in de werkgroep indicatorenontwikkeling.
7. De stuurgroep informeert belanghebbenden over de inhoudelijke wijzigingen in de kwaliteits-uitvraag. KIK-V informeert belanghebbenden over de nieuwe afsprakenset KIK-V en/of aangepaste modelgegevensset. Waar mogelijk en nodig wordt afgestemd over de gezamenlijke benadering van bepaalde doelgroepen, zoals softwareleveranciers.