---
title: "CBS"
description: "Centraal bureau voor de statistiek"
weight: 10
---

# CBS

Er zijn verschillende CBS bronnen met informatie over de zorg:
  

| Bron | Soort informatie |
|:-|:-|:-|
| StatLine | Informatie over de productiestatistieken van zorgaanbieders op het gebied van financiën en personeel | https://opendata.cbs.nl/statline/#/CBS/nl/dataset/84602NED/table?dl=28635 (financiën naar branche)<br>https://opendata.cbs.nl/statline/#/CBS/nl/dataset/83626NED/table?dl=2E2B8 (financiën en personeel) |
| Monitor Langdurige Zorg | Cijfers over de langdurige zorg (o.a. indicaties en gebruik van zorg) | https://www.monitorlangdurigezorg.nl/ |
| Arbeidsmarkt Zorg en Welzijn StatLine | Arbeidsmarktcijfers van de sector Zorg en Welzijn | https://azwstatline.cbs.nl/#/AZW/nl/ |

## Statline

In de StatLine staan de volgende gegevens m.b.t. personeelssamenstelling:

- Opbrengsten Wlz
- Personeelskosten

{{< hint info >}}
Deze bron hanteert de volledige Wlz zorg en niet de afbakening die het Kwaliteitskader kent (ZZP 4 t/m 10). Hierdoor kan de scope van deze gegevens afwijken van de scope van het Kwaliteitskader.
{{< /hint >}}

## Monitor Langdurige Zorg

In de Monitor Langdurige Zorg staat het aantal Wlz indicaties dat is afgegeven door het CIZ. Hierin vindt een uitsplitsing plaats naar soort aandoening (o.a. somatisch en psychogeriatrisch).

{{< hint info >}}
Deze bron hanteert de volledige Wlz zorg en niet de afbakening die het Kwaliteitskader kent (ZZP 4 t/m 10). Hierdoor kan de scope van deze gegevens afwijken van de scope van het Kwaliteitskader.
{{< /hint >}}

## Arbeidsmarkt Zorg en Welzijn StatLine

In de AZW staan de volgende gegevens m.b.t. personeelssamenstelling:

- Bedrijfsgrootte / dienstverband
- Dienstverband (vast, tijdelijk, anders, cliëntgebonden functies)
- Aantal vrijwilligers in organisatie: dit is gespecificeerd naar branche
- Ziekteverzuimpercentage: definitie van CBS/Vernet wordt gehanteerd. Er wordt wel gespecificeerd naar branche.
- Gemiddelde verzuimfrequentie: definitie van CBS/Vernet wordt gehanteerd. Er wordt niet gespecificeerd naar branche.
- Instroom: het gaat hierbij om instroom in een bepaalde zorgsector en geen instroom bij een specifieke organisatie
- Uitstroom: het gaat hierbij om uitstroom in een bepaalde zorgsector en geen uitstroom bij een specifieke organisatie

{{< hint info >}}
Voor deze bron van het CBS wordt gespecificeerd naar de branche VVT (en in sommige gevallen naar V&V). Hierdoor kan de scope van deze gegevens afwijken van de scope van het Kwaliteitskader (dat is beperkt tot verpleeghuiszorg).
{{< /hint >}}

## Arbeidsmarkt Zorg en Welzijn StatLine - enquête

Vanuit het Arbeidsmarkt Zorg en Welzijn programma wordt jaarlijks een enquête uitgevoerd onder werkgevers en werknemers in de zorg (https://azwstatline.cbs.nl/#/AZW/nl/navigatieScherm/thema?themaNr=24075).
  

In deze uitvraag worden o.a. de volgende personeelsgegevens uitgevraagd:

- Aantal medewerkers;
- Aantal FTE;
- Aantal vaste contracten, tijdelijke contracten en andere (ZZP bijv.);
- Aantal cliëntgebonden medewerkers;
- Aantal vrijwilligers;
- In- en uitstroom.

{{< hint info >}}
De definities binnen deze uitvraag komen overeen met die van andere uitvragen. Echter, deze vraag wordt gesteld aan de werkgevers **over de gehele breedte van het zorgveld**, dus niet specifiek voor hun rol in de verpleeghuissector. Voorbeeld: cliëntgebonden medewerkers worden Wlz breed gevraagd en hierin wordt geen uitsplitsing gemaakt binnen de Wlz.
{{< /hint >}}