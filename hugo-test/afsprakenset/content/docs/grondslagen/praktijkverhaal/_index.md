---
title: "Praktijkverhaal"
description: "Het praktijkverhaal beschrijft op toegankelijke wijze de praktische toepassing en toegevoegde waarde van de afsprakenset voor de verschillende belanghebbenden."
bookCollapseSection: true
weight: 2
---

# Praktijkverhaal

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

{{<section>}}