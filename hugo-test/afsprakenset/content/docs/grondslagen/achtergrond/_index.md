---
title: "Achtergrond"
description: "De achtergrond beschrijft de belemmeringen in het huidige speelveld en introduceert de afsprakenset als oplossing."
weight: 1
---

# Achtergrond

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

Een goede informatievoorziening kan (veelal) niet door één van de bij KIK-V betrokken partijen worden vormgegeven. Met andere woorden, men heeft elkaar nodig. Een gemeenschappelijke aanpak is daarom van belang.

## Mogelijkheden voor samenwerking en afspraken

![Vraag en antwoord spel - nu](vraag-en-antwoord-spel(nu).png)

Er zijn verschillende geconstateerde situaties benoemd door betrokken partijen in de eerste fase van het programma KIK-V en uit nulmetingen bij verpleeghuizen waarin gegevensuitwisseling mogelijk efficiënter en effectiever kan plaatsvinden. In de kern komt het erop neer dat informatievragen uit de keten rond kwaliteit, keuze en verantwoording en bijbehorende informatiestromen niet op elkaar afgestemd zijn:

- Zorgaanbieder (of andere aanbieders in de keten) moet steeds de informatievraag beantwoorden, in plaats van de vragende afnemer zelf;
- Dezelfde gegevens worden door verschillende partijen net anders gevraagd, in andere formats met andere definities. Veelal weten afnemers niet van elkaar wat zij uitvragen;
- Er is geen transparantie in uiteindelijk gebruik van die gegevens en de conclusies die daaruit worden getrokken, waarmee een gebrek aan vertrouwen ontstaat in het gebruik van de gegevens;
- Informatie-uitvragen zijn veelal incidenteel op verschillende momenten in de tijd. Planning van verschillende uitvragen zijn onvoldoende op elkaar afgestemd. Uitvragen veranderen ook (in opzet en gevraagde informatie) met onvoldoende tijd om hier invulling aan te geven richting processen en systemen;
- Het is veel werk om gevraagde gegevens uit bronsystemen beschikbaar te krijgen in de gevraagde definities en formats. Informatievragen sluiten niet goed aan op gegevens die in het operationele proces worden geregistreerd. Daarnaast zijn gevraagde indicatoren multi interpretabel;
- Om uitspraken te kunnen doen over kwaliteit op basis van de gegevens is in de meeste gevallen context en uitleg nodig bij deze gegevens. Veel relevante informatie is bovendien kwalitatief en/of narratief;
- Er is onduidelijkheid over juridische kaders, privacy en beveiliging bij uitwisseling van gegevens ten behoeve van kwaliteitsinformatie;
- Er zijn twijfels over de actualiteit en kwaliteit van de gegevens. Bestaande bronnen of gegevensverzamelingen worden niet ingezet;
- Er is een verschil tussen (opvatting over) kwaliteit(sprocessen) bij zorgaanbieder en andere ketenpartijen, waardoor eigen kwaliteitsregistratie veelal niet overeenkomt met gevraagde gegevens door andere partijen;
- Er worden discussies gevoerd over wat zinvolle kwaliteitsinformatie is en de aanvaardbaarheid van de administratieve lasten die daarmee gepaard gaan;
- Niet alle informatie over de kwaliteit van verpleeghuiszorg is openbaar of openbaar te maken;
- Veranderbereidheid van partijen om huidige belemmeringen op te lossen verschilt in de keten. Het gaat tenslotte om een bestaande situatie waarin veranderingen moeten worden vormgegeven.
  
Uit de huidige situatie kan het volgende worden geconcludeerd:

- *Vertrouwen*: partijen hebben onvoldoende vertrouwen in elkaar t.a.v. de verantwoording, in de kwaliteit van de gegevens en de duiding van de informatie. Dit komt ook door gebrek aan kennis over hoe andere partijen de opgevraagde gegevens duiden en gebruiken.
- *Interoperabiliteit*: gegevens zijn niet makkelijk van het ene naar het andere systeem over te zetten. Systemen spreken niet dezelfde taal.
- *Eigen opdracht*: partijen hebben hun eigen opdrachten waar ze individueel op ‘afgerekend’ worden. Zo komt interne stuurinformatie niet overeen met de externe kwaliteitsinformatie of vragen ketenpartijen toch eigen gegevens uit om hun eigen opdracht uit te kunnen voeren.
- *Voorspelbaarheid*: het handelen van de uitvragende partijen wordt als onvoorspelbaar ervaren. Dit komt door bijvoorbeeld het ad hoc uitvragen van informatie waar zorgaanbieders niet op voorbereid zijn of het uitvragen van informatie zonder duidelijke reden.
  
Dit leidt tot de conclusie dat informatie-uitvragen over de verpleeghuiszorg, van verschillende organisaties aan instellingen die verpleeghuiszorg leveren (gemiddeld genomen):

- Niet effectief genoeg zijn: de gevraagde gegevens zijn niet altijd de beste voor het gebruiksdoel, en de gegevenskwaliteit laat te wensen over.
- Niet efficiënt genoeg zijn: de administratieve lasten (kosten voor het registreren en uitwisselen van de gegevens) staan niet in verhouding tot de waarde die de gegevens opleveren.
  
Dit komt doordat er onvoldoende hergebruik van door de zorgaanbieder geregistreerde informatie en afstemming plaatsvindt. Informatie-uitvragen zijn nu enkelvoudig en eenzijdig. Dit leidt tot:

- Beperkingen in kwaliteitsverbetering, toezicht, rechtmatige en doelmatige bekostiging, en geïnformeerde keuzes;
- Een reductie van de tijd voor zorg.

## Afsprakenset als oplossing

KIK-V werkt aan de opzet van een samenwerkingsstructuur waarbinnen de ketenpartijen gezamenlijk afspraken kunnen ontwikkelen, vastleggen en uitproberen over de informatievoorziening in de sector verpleeghuiszorg. Deze samenwerkingsstructuur is duurzaam en werkt door als het programma ten einde is.
  
De afspraken die ontwikkeld en vastgelegd worden, dienen de huidige situatie van de informatievoorziening te verbeteren. De mogelijkheden voor samenwerking in de informatievoorziening die de ketenpartijen samen hebben geconstateerd, worden zoveel mogelijk opgepakt. De resultaten van de nulmeting zijn daarvan een voorbeeld.
  
Met een set van afspraken wordt de gezamenlijke wens tot verbetering van de huidige situatie benadrukt; dit zorgt voor meer draagvlak en kwaliteit dan wanneer de afspraken zouden worden opgelegd door de afnemers met een publieke taak.

## Kenmerken van het speelveld

Voor de uitwerking van de afsprakenset is het van belang rekening te houden met de volgende kenmerken van het speelveld:

- Informatie over de verpleeghuiszorg is niet beperkt tot kwaliteits- en keuze-informatie;
- Informatie over de verpleeghuiszorg kan ook van andere bronhouders dan de huidige ketenpartijen komen;
- Informatie over kwaliteit is niet beperkt tot het Kwaliteitskader Verpleeghuiszorg;
- Informatie-uitwisseling heeft mede betrekking op gegevens die niet-openbaar zijn;
- Informatie-uitwisseling is niet beperkt tot gestructureerde data, maar omvat ook kwalitatieve en narratieve informatie;
- Veel instellingen voor verpleeghuiszorg leveren ook andere zorg;
- Instellingen voor verpleeghuiszorg gebruiken vaak verschillende relevante informatiesystemen (bijvoorbeeld gericht op zorginhoud versus bedrijfsvoering);
- Aanbieders en afnemers van kwaliteitsinformatie verkeren vaak in een ongelijkwaardige positie;
- Partijen vinden het moeilijk om hun (toekomstige) informatiebehoefte te overzien en te onderbouwen.
- In het ontwerpproces dient door de ketenpartijen in gezamenlijkheid bepaald te worden in hoeverre met bovenstaande punten wel/niet rekening wordt gehouden bij het maken van de afspraken.
