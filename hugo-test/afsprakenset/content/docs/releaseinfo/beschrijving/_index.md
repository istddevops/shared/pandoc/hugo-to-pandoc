---
title: "Release- en versiebeschrijving"
description: "De releasebeschrijving beschrijft de belangrijkste kenmerken van de release. De versie betreft de versie van de release en duidt aan in welk stadium van ontwikkeling of besluitvorming de release zich bevindt. Een release die is vastgesteld heeft altijd versie 1.0. Hogere versienummers zijn alleen mogelijk als er documentatiecorrecties worden doorgevoerd. Inhoudelijke wijzigingen op een al vastgestelde release leiden altijd tot een nieuwe release."
weight: 1
---

# Release- en versiebeschrijving

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

| Release | 1.1 |
|:-|:-|
| Versie | 1.1 versie 0.9 ter vaststelling door de Ketenraad KIK-V |
| Doel | Release 1.1 betreft de tweede publicatie van de afsprakenset en bevat met name de benodigde uitbreidingen van de model gegevensset ten behoeve van de implementatie en het gebruik van het [uitwisselprofiel ODB](https://kik-v.gitlab.io/uitwisselprofielen/uitwisselprofiel-odb). |
| Doelgroep | <ul><li>Ketenpartijen KIK-V</li><li>Zorgaanbieders</li><li>verpleeghuiszorg</li><li>andere geïnteresseerden in de KIK-V Afsprakenset</li></ul> |
| Totstandkoming | De ontwikkeling van release 1.1 is uitgevoerd onder leiding van het project Afspraken binnen het programma KIK-V, in samenwerking met de belanghebbende partijen. Release 1.1 wordt vastgesteld door de Ketenraad KIK-V op basis van versie 0.9. |
| Inwerkingtreding | 25 juni 2021 |
| Operationeel toepassingsgebied | De afsprakenset release 1.1 is breed inzetbaar en dient als basis voor implementatie door de ketenpartijen en zorgaanbieders, met name voor de toepassing van het [uitwisselprofiel ODB](https://kik-v.gitlab.io/uitwisselprofielen/uitwisselprofiel-odb). |
| Status | Ter vaststelling door Ketenraad KIK-V |
| Functionele scope | Release 1.0 omvat: <ul><li>Hetgeen gepubliceerd in release 1.0</li><li>Wijzigingen en aanvullingen op de model gegevensset ten behoeve van het [uitwisselprofiel ODB](https://kik-v.gitlab.io/uitwisselprofielen/uitwisselprofiel-odb)</li></ul> |
| Licentie | [Creative Commons: Naamsvermelding-GeenAfgeleideWerken 4.0 Internationaal](https://creativecommons.org/licenses/by-nd/4.0/). |