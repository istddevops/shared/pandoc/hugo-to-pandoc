---
title: "Publicatie"
description: "Publicaties modelgegevensset"
weight: 30
---

# {{< param description >}}

{{< hint info >}}
Deze pagina bevat de publicatie van de modelgegevensset. 
{{< /hint >}}

De modelgegevensset is beschreven in een VPH-ontologie[^1]:

- [Publicatie VPH Generiek](http://purl.org/ozo/vph-g)
- [Publicatie VPH Personeel](http://purl.org/ozo/vph-pers)
- [Publicatie VPH Financieel](http://purl.org/ozo/vph-fin)

[^1]: VPH staat voor Verpleeghuis en Ontologie is het kennismodel over het Verpleeghuis domein wat volgens de OWL2-standaard is vastgelegd.
