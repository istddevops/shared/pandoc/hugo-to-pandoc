---
title: "Modelgegevensset"
bookCollapseSection: true
weight: 5
---

# Modelgegevensset

{{< hint info >}}
De sectie Modelgegevensset beschrijft de opzet en de inhoud van de modelgegevensset. 
{{< /hint >}}

De afsprakenset maakt voor het definiëren van de gegevens, die aanbieders toepassen in het formuleren van de antwoorden op informatievragen omschreven in het uitwisselprofiel, gebruik van de modelgegevensset. De modelgegevensset is het resultaat van het matchingsproces, een afstemming tussen informatiebehoefte en beschikbare gegevens bij de zorgaanbieder. In de modelgegevensset zijn tevens de definities opgenomen van de gehanteerde gegevens.
  

De modelgegevensset wordt gebruikt om bij de gegevensuitwisseling invulling te kunnen geven aan (de concepten uit) de informatievragen van afnemers. De set bevat de semantiek van de gegevens die zorgaanbieders beschikbaar stellen voor het beantwoorden van de vragen. De semantiek is voor alle aanbieders en afnemers uniform in een Ontologie (methode om de betekenis van concepten binnen een specifieke context te beschrijven) vastgelegd.
  

Aanbieders kunnen, al dan niet in overleg met hun softwareleveranciers, zelf bepalen hoe zij vanuit de brondata tot de juiste afbeelding van de gegevens op de ontologie (voor de gebruikte modelgegevensset) komen. Aanbieders zijn zelf verantwoordelijk voor de wijze waarop zij dit doen. Dit geeft hen de ruimte om de koppeling tussen bronsystemen en de modelgegevensset op eigen wijze in te vullen. De uitkomst hiervan is de aanbieder-gegevensset.

{{<sectiontreelist>}}
