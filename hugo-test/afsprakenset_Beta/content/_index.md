---
title: Introductie
type: docs
---

{{< hint danger >}}
Dit is een **CONCEPT PUBLICATIE** (in afwachting goedkeuring Ketenraad KIK-V)
{{< /hint >}}

# Afsprakenset KIK-V

Het programma Keteninformatie Kwaliteit Verpleeghuiszorg, KIK-V, draagt bij aan de verbetering van de gegevensuitwisseling over kwaliteit in de verpleeghuiszorg. Binnen KIK-V werken instellingen voor verpleeghuiszorg (als aanbieder) en andere partijen met een publieke taak (als afnemer) samen om deze gegevensuitwisseling effectiever en efficiënter te maken. De afspraken landen in de voorliggende afsprakenset KIK-V.
De afspraken dragen bij aan het onderlinge vertrouwen en faciliteren een nieuwe, meer gezamenlijke, manier van werken. Ook bewerkstelligen de afspraken onderlinge interoperabiliteit (elkaar begrijpen en gegevens kunnen uitwisselen). Omwille van de interoperabiliteit hebben de afspraken betrekking op alle lagen van het interoperabiliteitsmodel en de laagoverstijgende aspecten privacy, informatiebeveiliging en beheer. 
Inhoudelijk is kwaliteit afgebakend tot de thema's die vallen onder het Kwaliteitskader Verpleeghuiszorg. De afspraken hebben zowel betrekking op de uitwisseling van kwantitatieve en kwalitatieve gegevens.

Zie verder [opbouw afsprakenset](./docs/opbouw)
