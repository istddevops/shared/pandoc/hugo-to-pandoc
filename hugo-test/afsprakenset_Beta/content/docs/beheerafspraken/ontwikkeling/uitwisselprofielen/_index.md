---
title: "Uitwisselprofielen"
description: "Afspraken over het opstellen, wijzigen, vaststellen, publiceren en implementeren van uitwisselprofielen."
bookCollapseSection: true
weight: 1
---

# Ontwikkeling uitwisselprofielen

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

- De beheerorganisatie onderhoudt de uitwisselprofielen en richt hiervoor een wijzigingsproces in. Voor bepaalde uitwisselprofielen kunnen aanvullende afspraken zijn gemaakt met betrekking tot het wijzigingsproces. Dit geldt in ieder geval voor het uitwisselprofiel Zorginstituut ODB waarvoor [Procesafspraken stuurgroep kwaliteitskader voor uitwisselprofiel ODB](procesafspraken-odb) zijn gemaakt met de Stuurgroep Kwaliteitskader Verpleeghuiszorg. 
- De afnemer maakt een nieuwe of gewijzigde informatiebehoefte kenbaar bij de beheerorganisatie.
- De beheerorganisatie ziet toe op de logische samenhang van uitwisselprofielen en bepaalt samen met de afnemer of een (gewijzigde) informatiebehoefte een nieuw uitwisselprofiel behoeft of kan worden toegevoegd aan een bestaande.
- De beheerorganisatie is samen met de afnemer verantwoordelijk voor het vertalen van de informatiebehoefte naar een uitwisselprofiel. Middels het [Matchingsproces](../matchingproces) wordt op gestructureerde wijze de informatiebehoefte vertaald naar binnen KIK-V toepasbare informatievragen, concepten, definities en gegevenselementen. De beheerorganisatie ondersteunt in dit proces de afnemer waar nodig bij het (her)formuleren van zijn informatiebehoefte.
- De beheerorganisatie stelt samen met de afnemer uitwisselprofielen op volgens het [Modeluitwisselprofiel](../../../modeluitwisselprofiel).
- De beheerorganisatie faciliteert de benodigde afstemming en toetsing bij belanghebbende partijen.
- De beheerorganisatie faciliteert praktijktoetsing van de uitwisselprofielen. Uitwisselprofielen worden minimaal in samenhang in praktijk getoetst bij een eerste versie en/of bij grootschalige wijzigingen.
- De beheerorganisatie onderzoekt de impact van het uitwisselprofiel op de afsprakenset en de overige uitwisselprofielen (o.a. om een goede publicatie- en ingangsdatum te bepalen). De uitkomst wordt meegenomen in de besluitvorming.
- De beheerorganisatie faciliteert het besluitvormingsproces bij de Ketenraad KIK-V.
- De Ketenraad KIK-V stelt (majeure) wijzigingen aan uitwisselprofielen vast. Het Tactisch overleg KIK-V besluit over kleine en/of spoedwijzigingen en legt hierover verantwoording af aan de Ketenraad KIK-V.
- De beheerorganisatie is na besluitvorming verantwoordelijk voor het formeel doorvoeren van wijzigingen aan uitwisselprofielen.
- De beheerorganisatie publiceert het uitwisselprofiel op een voor aanbieders en afnemers toegankelijke plek. *De beheerorganisatie kent daarbij metadata toe aan het uitwisselprofiel* conform [Metadata uitwisselprofiel](metadata). 
- De beheerorganisatie voorziet in de benodigde informatievoorziening richting deelnemers ten behoeve van een soepele implementatie.
- De beheerorganisatie archiveert wijzigingsvoorstellen.
- De beheerorganisatie evalueert in samenspraak met de deelnemers zowel het proces voor de totstandkoming als de inhoud van het nieuwe uitwisselprofiel.
- De beheerorganisatie kan, waar nodig en gepast, totstandkoming en onderhoud van voorzieningen faciliteren die een rol spelen bij de gegevensuitwisseling zoals beschreven in de uitwisselprofielen.

{{<section>}}