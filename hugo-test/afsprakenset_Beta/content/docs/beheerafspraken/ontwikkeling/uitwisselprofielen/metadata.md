---
title: "Metadata"
description: "Beschrijving van de Vereiste metadata voor publicatie van een uitwisselprofiel."
weight: 10
---

# Metadata uitwisselprofiel

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

De beheerorganisatie kent metadata toe aan elk uitwisselprofiel dat wordt gepubliceerd conform onderstaande eisen.
  

Om de set metadata-elementen minimaal te houden en te standaardiseren zijn er een aantal (inter)nationale metadatastandaarden als basis gebruikt.  Deze standaarden zijn de [OWMS4.0](https://standaarden.overheid.nl/owms/terms) (Overheid.nl Web Metadata Standaard), [DCAT-AP-NL](https://data.overheid.nl/sites/default/files/uploaded_files/2018-02-21%20DCAT-AP-NL%20IPM%20Datasets%201.1%20concept.docx) en de [FAIR Data Point Specification](https://github.com/FAIRDataTeam/FAIRDataPoint/wiki/FAIR-Data-Point-Specification). Door deze standaarden als basis te gebruiken voldoet de afsprakenset voor de deelnemende overheidsorganisaties aan verplichte standaarden en is het mogelijk om aan het FAIR initiatief te voldoen.
  

In onderstaande tabel is er per rij een metadata-element opgenomen. De kolommen in de tabel staan voor:

- **Element**: ieder element bestaat uit een prefix (de verwijzing naar het gebruikte vocabulary) en de naam van het element uit dat vocabulary. Onderstaande tabel beschrijft de gebruikte vocabularies inclusief de bijbehorende namespace (identificerende plaats van de vocabulary)
- **Korte uitleg**: per element een korte uitleg, geparafraseerd aan de hand van de definities in de verschillende standaarden.
- **Uitwerking**: concept van de wijze waarop ieder element gevuld dient te worden.
- **Rationale**: omschrijving van de keuze voor de uitwerking in de huidige versie.
- **Reden inclusie**: beschrijft in welke standaard het element verplicht is. Voor de leesbaarheid is FAIR Data Point Specification afgekort tot FDPS. Recommended in DCAT-AP-NL betekent dat het element alleen verplicht opgenomen moet worden wanneer de partij over de informatie beschikt. In OWMS4.0 zijn de verplichte elementen opgenomen in de Kern en de optionele opgenomen in de Mantel.
- **Range**: beschrijft de technische vereiste vanuit de techniek. Dit overlapt gedeeltelijk met kolom ‘Uitwerking’.

  

| Prefix    | Namespace                                   |
|-----------|---------------------------------------------|
| dcat:     | http://www.w3.org/ns/dcat#                  |
| dct:      | http://purl.org/dc/terms/                   |
| fdp:      | http://rdf.biomantics.org/ontologies/fdp-o# |
| rdfs:     | http://www.w3.org/2000/01/rdf-schema#       |
| overheid: | http://standaarden.overheid.nl/owms/terms/  |

## Uitwisselptofiel

Ieder vastgesteld uitwisselprofiel wordt uitgegeven door de beheerorganisatie. De kans is aanzienlijk dat dit een (semi)overheidsorgansatie betreft en daarmee moet voldoen aan de geldende standaarden voor de Rijksoverheid. Bij digitale publicatie van documenten zijn overheidsorganisaties verplicht de OWMS4.0 aan te houden als metadatastandaard.

| Metadata element | Korte uitleg | Uitwerking | Rationale | Reden inclusie | Range |
|:-|:-|:-|:-|:-|:-|
| dct:identifier | De unieke identificatie van de een specifieke versie van het uitwisselprofiel. | URI: beheerorganisatie/GUID | Identifiers zijn URI's. Dit houdt in dat deze uniek en persistent zijn. Het heeft onze voorkeur deze door de beheerorganisatie uit te laten geven of aan te sluiten bij de wijze waarop de Rijksoverheid dit in wil richten. | Kern in OWMS4.0 | KIK-V: URI |
| dct:title | De naam van het  uitwisselprofiel. | Uitwisselprofiel KIK-V + dct:temporal + dcat:keyword (gebonden partijen) | Kern in OWMS4.0 | |
| dct:description | Beschrijving van het uitwisselprofiel. | Vrije tekst, nog in te vullen | | Mantel in OWMS4.0 | OWMS4.0: rdfs:Literal |
| dct:modified | De datum waarop het uitwisselprofiel voor het laatst is gewijzigd. | Datum | Laatste datum vaststelling | Kern in OWMS4.0 | OWMS4.0: xsd:date |
| dct:language | De taal waarin de inhoud van het uitwisselprofiel is uitgedrukt. | Nederlands | |Kern in OWMS4.0 | OWMS4.0: xsd:language |
| overheid:authority | Overheidsorganisatie die de wettelijke verantwoordelijkheid draagt voor de inhoud (strekking) van het uitwisselprofiel. | Beheerorganisatie | |  Kern in OWMS4.0 | OWMS4.0: overheid:Agent gebaseerd op  dct:Agent |
| dct:publisher | De beheerorganisatie die verantwoordelijk is voor de publicatie van het uitwisselprofiel. | Beheerorganisatie | | Mantel in OWMS4.0 | OWMS4.0: overheid:Agent gebaseerd op dct:Agent |
| dct:spatial | Geografisch gebied waar het uitwisselprofiel betrekking op heeft.| Nederland | | Kern in OWMS4.0 | OWMS4.0: overheid:Locatie gebaseerd op dct:Location |
| dct:temporal | Periode waar binnen het uitwisselprofiel geldig is. | Geldigheidsduur modelgegevensset | | Kern in OWMS4.0 | OWMS4.0: xsd:date voor begindatum en einddatum |
| dct:hasVersion | De versie van het model-uitwisselprofiel waarop dit profiel gebaseerd is | | | Mantel in OWMS4.0 |

## Filtercriteria

Voor de beheerorganisatie gaat het van belang zijn om selecties van gepubliceerde uitwisselprofielen te kunnen tonen aan relevante partijen. Hierdoor raakt het overzicht niet kwijt op het moment dat er een grote hoeveelheid uitwisselprofielen gepubliceerd zijn. De filtercriteria zijn gebaseerd op en een uitbreiding van bovenstaande metadata.

| Filter | Technische invulling | Uitleg |
|:-|:-|:-|
| Actief | Ja/Nee | De optie om alleen actieve of inactieve uitwisselprofielen te tonen. Gebaseerd op de geldigheidsduur van het uitwisselprofiel. |
| Grondslag A | Wettelijk Ja/Nee | De optie om te selecteren of het uitwisselprofiel een grondslag heeft uit een wettelijk kader of gebaseerd is op onderlinge afspraken. |
| Grondslag B | Keuzelijst wet(sartikelen) | De optie om de relevante wetsartikelen te selecteren op het moment dat de grondslag van een uitwisselprofiel gebaseerd is op een wettelijk kader. |
| *Doel* | | *Momenteel niet gestandaardiseerd genoeg om te kunnen gebruiken.* |
| Moment van gegevensuitwisseling / Peildata / Peilperiode | Datum(bereiken) | Wellicht kan het interessant zijn op basis van deze datums te kunnen filteren. Vanuit de metadata wordt nu alleen de datum bijgehouden wanneer een uitwisselprofiel voor het laatst gewijzigd is en wat de geldigheidsperiode is. |
| Wijze van aanlevering | Keuzelijst | De optie om te selecteren op de wijze van gegevensuitwisseling van het uitwisselprofiel (de technische operabiliteit). |
| Betrokken partijen | Keuzelijst afnemers/aanbieders | De optie om te selecteren op basis van de betrokken partijen bij een uitwisselprofiel. |
