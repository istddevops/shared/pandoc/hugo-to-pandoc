---
title: "Bestaande bronnen"
description: "Op onderstaande pagina’s staan diverse bestaande (openbare) databronnen die betrekking hebben op kwaliteitsgegevens in de zorg. Deze bronnen bevat mogelijk al informatie dat niet opnieuw uitgevraagd hoeft te worden bij zorgaanbieders via een uitwisselprofiel."
bookCollapseSection: true
weight: 1
---

# Bestaande (openbare) bronnen

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

Om te bepalen of deze informatie toegepast kan worden door een afnemer, volgt meer informatie over de inhoud van deze bronnen. Het gaat hierbij om de volgende bronnen:

{{< section >}}

## Toetsingscriteria (openbare) bronnen

Een databron kan worden toegevoegd aan de set van afspraken wanneer deze als van toegevoegde waarde wordt beoordeeld door de beheerorganisatie. De beoordeling vindt plaats aan de hand van een afweging van de toegevoegde waarde enerzijds, en onderstaande randvoorwaardelijke uitgangspunten anderzijds. De uitgangspunten zijn een indicatie of een informatiebron van waarde is, de uiteindelijke afweging is hierin doorslaggevend voor de opname van de informatiebron in de afsprakenset.

### Randvoorwaardelijke uitgangspunten:

- De informatiebron is reeds in gebruik door één of meerdere partijen binnen het programma KIK-V;
- De informatiebron is valide: in de informatiebron staat de methode omtrent informatieverzameling beschreven;
- De informatiebron is betrouwbaar;
- De informatiebron is actueel: de informatiebron bevat actuele gegevens en zijn relevant voor het kader waarin de informatie uit wordt ontsloten;
- De informatiebron is volledig: de informatiebron is representatief voor datgene dat de informatiebron presenteert;
- De informatiebron wordt beheerd: de informatiebron moet door een persoon of organisatie worden beheerd.
