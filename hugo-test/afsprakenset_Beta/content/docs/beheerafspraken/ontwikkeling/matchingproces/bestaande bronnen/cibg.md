---
title: "CIBG"
description: "Jaarverantwoording Zorg"
weight: 30
---

# CIBG (Jaarverantwoording Zorg)

Veel organisaties in het zorgveld leggen jaarlijks verantwoording af over hun prestaties. Zo laten zij zien hoe zij publiek geld besteden. En hoe zij de zorg, hulp- of dienstverlening en ondersteuning hebben georganiseerd. De meeste gegevens zijn openbaar en worden door diverse (overheids)organsiaties gebruikt voor bijvoorbeeld toezicht en beleidsvorming.
  

De Jaarverantwoording Zorg bevat de volgende informatie:

- Gegevens over medewerkers: wel- of niet cliëntgebonden personeel
- Gegevens over in- en uitstroom van medewerkers, ingehuurde zorgverleners, zelfstandigen en vrijwilligers
- Gegevens over aantal fte op einde boekjaar: medewerkers, ingehuurde zorgverleners, zelfstandigen en vrijwilligers
- Ziekteverzuimpercentage
- Aantal stagiaires

Meer informatie: https://www.jaarverantwoordingzorg.nl
