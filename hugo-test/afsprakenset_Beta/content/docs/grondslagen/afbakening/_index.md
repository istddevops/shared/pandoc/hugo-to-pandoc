---
title: "Afbakening"
description: "Afbakening beschrijft de keuzes die zijn gemaakt om de eerste uitwerking van de afsprakenset af te bakenen."
weight: 6
---

# Afbakening eerste conceptversie afsprakenset

{{< hint info >}}
{{< param description >}}
{{< /hint >}}

Het Kwaliteitskader Verpleeghuiszorg, en de gegevensuitwisseling over de thema’s hierin, betreft een veelomvattend speelveld. Het gaat om meerdere ketenpartijen, om diverse processen in de langdurige zorg en meerdere, diverse soorten gegevens uit vele verschillende informatiesystemen. Om toch al snel een eerste conceptversie van afspraken op te kunnen leveren, is een scherpe afbakening van het speelveld voor een eerste stap van belang. Afbakening vindt bijvoorbeeld plaats door middel van keuzes voor specifieke (juridische) domeinen en/of doelgroep, bepaalde functionaliteit/use cases en technische keuzes voor de start. De afbakening is vooral gericht op de inhoud van de afspraken. De structuur waarbinnen de afspraken worden opgesteld en waarbinnen besluitvorming plaatsvindt, wordt zoveel mogelijk al generiek opgezet. Hierdoor kunnen uitbreidingen in de nabije toekomst snel op dezelfde manier worden vormgegeven.
  
Afbakening van de eerste conceptversie voor de afsprakenset in afstemming met de ketenpartijen:
  
| Nr. | Beschrijving |
|:-|:-|
| A1 | Het maken van afspraken is in de eerste versie gericht op:<ul><li>**a.**	De informatievoorziening met betrekking tot het kwaliteitskader voor de verpleeghuiszorg en de hierbij betrokken ketenpartijen.</li><li>**b.**	Daarbinnen is ervoor gekozen om te starten met de thema’s personeelssamenstelling en basisveiligheid en de verbetering van de gegevensuitwisseling hierover.</li></ul> |
| A2 | Er is ook gekozen voor een afbakening in het aantal mogelijke gegevensstromen tussen partijen waarvoor afspraken worden beschreven. Het voorstel is om het eerste ontwerp, de eerste uitwisselprofielen, te richten op de uitvraag van gegevens bij de zorgaanbieder, het verpleeghuis, door de betrokken ketenpartijen, te starten met de uitvraag ODB en door zorgkantoren. Dat betekent dat ketenpartijen als bron voor gegevens over kwaliteit, bijvoorbeeld in de onderlinge gegevensuitwisseling tussen de IGJ en NZA, voor de eerste conceptversie buiten scope blijft. |
| A3 | Het ontwerpproces start bij de onderlinge afstemming over de gewenste samenwerking in de gegevensuitwisseling, het coördinatievraagstuk. Daarmee wordt tussen betrokken partijen eerst kennis uitgewisseld, bijvoorbeeld over de huidige situatie en de gewenste situatie, vertrouwen opgebouwd en samenwerking opgestart. Het bouwt voort op de relatie die er al ligt tussen de ketenpartijen, onder meer bekrachtigd via de samenwerkingsafspraken. Daarna volgen eventuele afspraken over de techniek om dat te ondersteunen. In de uitwerking van de techniek volgen we de ontwikkelingen binnen het zorgdomein, onder andere gecoördineerd door het Zorginstituut, zoals bijvoorbeeld de voorstellen voor een netwerkarchitectuur. Het principe hiervan is dat alle partijen met elkaar zijn verbonden in een netwerk, dat data gereed wordt gezet in zogenoemde datastations en afnemers deze data kunnen ophalen, dan wel een berekening laten uitvoeren en het antwoord kunnen ophalen. |
| A4 | Voor de inhoud van de uitwisseling van kwaliteitsinformatie geldt dat de afspraken over het gebruik ervan binnen scope zijn, maar de afspraken over inhoud (wat is kwaliteitsinformatie?) buiten scope. |
