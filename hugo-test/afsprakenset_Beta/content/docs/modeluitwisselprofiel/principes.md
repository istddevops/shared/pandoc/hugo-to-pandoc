---
title: "Principes"
weight: 10
---

## Principes KIK-V uitwisselprofielen

### Principe 1
Binnen KIK-V wordt alleen op basis van goedgekeurde uitwisselprofielen uitgewisseld.

### Principe 2
Uitwisselprofielen sluiten aan bij de binnen KIK-V vastgestelde standaardafspraken.

### Principe 3
Uitwisselprofielen worden opgesteld per afnemer en specifiek doel, omdat de operationele afspraken in het uitwisselprofiel mede afhankelijk zijn van het doel van de afnemer. 

### Principe 4
Een uitwisselprofiel bevat per afnemer en specifiek doel een set van een of meer gevalideerde informatievragen en -antwoorden.

### Principe 5
KIK-V maakt in de basis afspraken over 1-op-n situaties (1 afnemer op meerdere aanbieders). Een uitwisselprofiel is daarmee in de regel van toepassing op meerdere, dan wel alle, aanbieders.

### Principe 6
De uitwisseling vindt plaats conform de afspraken in het profiel.

### Principe 7
Afnemers en aanbieders kunnen in het geval van geschillen bij de operationele gegevensuitwisseling terugvallen op het - uitwisselprofiel. 

### Principe 8
Een uitwisselprofiel bevat afspraken op verschillende niveaus van interoperabiliteit, zie ook: [Nictiz / Standaardisatie / Interoperabiliteit](https://www.nictiz.nl/standaardisatie/interoperabiliteit).

### Principe 9
Voor alle in- en exclusiecriteria die in het uitwisselprofiel worden opgenomen geldt dat zoveel mogelijk wordt aangesloten op in- en exclusiecriteria uit reeds beschikbare uitwisselprofielen. 
