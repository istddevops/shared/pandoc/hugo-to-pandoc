---
title: "WKKGZ"
bookToc: false
description: "Wet kwaliteit, klachten en geschillen zorg"
weight: 10
---

# **[{{< param description >}}](https://wetten.overheid.nl/BWBR0037173/2020-01-01)**

Geldend van 01-01-2020 t/m heden

{{< columns >}}

**Relevante artikelen**

*Artikel 7*
  
1. De zorgaanbieder draagt zorg voor systematische bewaking, beheersing en verbetering van de kwaliteit van de zorg.
2. De verplichting van het eerste lid houdt, de aard en omvang van de zorgverlening in aanmerking genomen, in:
    - a. het op systematische wijze verzamelen en registreren van gegevens betreffende de kwaliteit van de zorg op zodanige wijze dat de gegevens voor eenieder vergelijkbaar zijn met gegevens van andere zorgaanbieders van dezelfde categorie;
    - b. het aan de hand van de gegevens, bedoeld in onderdeel a, op systematische wijze toetsen of de wijze van uitvoering van artikel 3 leidt tot goede zorg;
    - c. het op basis van de uitkomst van de toetsing, bedoeld in onderdeel b, zo nodig veranderen van de wijze waarop artikel 3 wordt uitgevoerd.

<--->

**Toepassing voor de afspraken**

*Artikel 7 van de Wet kwaliteit, klachten en geschillen zorg (Wkkgz)* verplicht zorgaanbieders om aan systematische bewaking, beheersing en verbetering van de zorg te doen, onder meer door het op systematische wijze verzamelen en registreren van kwaliteitsgegevens op een wijze die ze vergelijkbaar maakt met die van andere zorgaanbieders in dezelfde categorie. Het artikel biedt een grondslag voor de opname van persoonsgegevens in de aanbiedergegevensset en de daaropvolgende aggregatie. Dit omdat het artikel spreekt van “systematisch” en “vergelijkbaar”. Daar vloeit een transformatie als die naar de aanbiedergegevensset conform het gegevenswoordenboek eigenlijk al uit voort.

{{< /columns >}}