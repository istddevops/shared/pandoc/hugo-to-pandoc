---
title: "RV-WTZi"
bookToc: false
description: "Regeling verslaggeving WTZi"
weight: 80
---

# **[{{< param description >}}](https://wetten.overheid.nl/BWBR0019252/2019-11-20#Artikel9)**

Geldend van 20-11-2019 t/m heden

{{< columns >}}

**Relevante artikelen**

*Artikel 9 lid 1*
  

Het bestuur van een zorginstelling levert de Jaarverantwoording Zorg vóór 1 juni van het jaar, volgend op het verslagjaar, met gebruikmaking van het elektronische platform DigiMV, aan bij het Centraal Informatiepunt Beroepen Gezondheidszorg.
  
<--->

**Toepassing voor de afspraken**

Met *artikel 9 lid 1* wordt een nadere invulling gegeven aan artikel 15 van het uitvoeringsbesluit WTZi. Met het artikel wordt geregeld dat het CIBG het agentschap van VWS is waar de gegevens moeten worden aangeleverd, verwerkt en weer openbaar beschikbaar worden gesteld.

{{< /columns >}}
