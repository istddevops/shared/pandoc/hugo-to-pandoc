---
title: "UB-WTZi"
bookToc: false
description: "Uitvoeringsbesluit WTZi"
weight: 70
---

# **[{{< param description >}}](https://wetten.overheid.nl/BWBR0018983/2018-07-01)**

Geldend van 02-04-2019 t/m heden

{{< columns >}}

**Relevante artikelen**

*Artikel 15*
  

Het bestuur van een instelling stelt overeenkomstig door Onze Minister, voor zoveel nodig in overeenstemming met Onze Ministers die het mede aangaat, te stellen regelen de begroting, de balans en de resultatenrekening alsmede de daarbij behorende toelichting met betrekking tot de instelling vast en legt volledige afschriften daarvan ter inzage voor een ieder ter plaatse, door Onze Minister te bepalen.
  

*Artikel 16*
  

Het bestuur van een instelling, behorende tot een bij algemene maatregel van bestuur aangewezen categorie, verstrekt aan Onze Minister of aan een bij die maatregel aangewezen bestuursorgaan de bij of krachtens die maatregel omschreven gegevens betreffende de exploitatie van de instelling.
  
<--->

**Toepassing voor de afspraken**

In het *Uitvoeringsbesluit WTZi* is voor de Jaarverantwoording Zorg de grondslag geregeld voor het aanleveren van gegevens door een zorginstelling en het verwerken van deze gegevens door Onze Minister.

{{< /columns >}}

