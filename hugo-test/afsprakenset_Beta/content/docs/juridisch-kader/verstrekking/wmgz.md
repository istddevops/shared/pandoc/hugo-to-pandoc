---
title: "WMGZ"
bookToc: false
description: "Wet marktordening gezondheidszorg"
weight: 40
---

# **[{{< param description >}}](https://wetten.overheid.nl/BWBR0020078/2019-07-01)**

Geldend van 01-07-2019 t/m heden

{{< columns >}}

**Relevante artikelen**

*Artikel 60*

1. In dit hoofdstuk worden persoonsgegevens onderscheiden in:
    - a. identificerende persoonsgegevens,
    - b. medische persoonsgegevens,
    - c. strafrechtelijke persoonsgegevens.
2. Onder identificerende persoonsgegevens wordt verstaan:
    - a. naam, adres, woonplaats, postadres;
    - b. geboortedatum en geslacht;
    - c. administratieve gegevens, zoals nummers van bank-, giro- en creditcard, gegevens uit de basisregistratie personen en registratie ingevolge de Wet op de beroepen in de individuele gezondheidszorg.
3. Onder medische persoonsgegevens wordt in het kader van deze wet verstaan de gegevens over gezondheid als bedoeld in artikel 4, onderdeel 15 van de Algemene verordening gegevensbescherming.
4. Onder strafrechtelijke persoonsgegevens wordt verstaan persoonsgegevens van strafrechtelijke aard als bedoeld in paragraaf 3.2 van de Uitvoeringswet Algemene verordening gegevensbescherming en persoonsgegevens betreffende onrechtmatig of hinderlijk gedrag in verband met een opgelegd verbod naar aanleiding van dat gedrag.
  
*Artikel 61*

1. Een ieder is gehouden desgevraagd aan de zorgautoriteit of aan een daartoe door deze aangewezen persoon, verder in dit artikel aan te duiden als vrager, kosteloos en met inachtneming van het bepaalde krachtens artikel 65:
    - a. de gegevens en inlichtingen te verstrekken welke redelijkerwijs voor de uitvoering van deze wet van belang kunnen zijn;
    - b. de boeken, bescheiden en andere gegevensdragers of de inhoud daarvan – zulks ter keuze van de vrager – waarvan de raadpleging redelijkerwijs van belang kan zijn voor de vaststelling van de feiten welke invloed kunnen uitoefenen op de uitvoering van deze wet, voor dit doel beschikbaar te stellen.
2. Ingeval deze wet aangelegenheden van een derde aanmerkt als aangelegenheden van degene die op grond van het eerste lid inlichtingenplichtig is, gelden, voor zover het deze aangelegenheden betreft, gelijke verplichtingen voor de derde.
3. De in het eerste lid, onderdeel b, bedoelde verplichting geldt onverminderd voor een derde bij wie zich gegevensdragers bevinden van degene die gehouden is deze, of de inhoud daarvan, aan de vrager voor raadpleging beschikbaar te stellen.
4. De vrager stelt degene wiens gegevensdragers hij bij een derde voor raadpleging vordert, gelijktijdig hiervan in kennis.
5. De gegevens en inlichtingen dienen duidelijk, stellig en zonder voorbehoud te worden verstrekt, mondeling, schriftelijk of op andere wijze – zulks ter keuze van de vrager – en binnen een door de vrager te stellen termijn.
6. Toegelaten moet worden dat kopieën, leesbare afdrukken of uittreksels worden gemaakt van de voor raadpleging beschikbaar gestelde gegevensdragers of de inhoud daarvan.
7. De verplichting van het eerste of tweede lid geldt niet indien de betrokkene de gevraagde gegevens of inlichtingen reeds aan een ander bestuursorgaan heeft verstrekt en zij door dat bestuursorgaan aan de zorgautoriteit verstrekt kunnen worden.
  
*Artikel 62*
  
1. De zorgautoriteit kan, met inachtneming van het bepaalde krachtens artikel 65, regels stellen, inhoudende welke gegevens en inlichtingen regelmatig moeten worden verstrekt dan wel onder welke omstandigheden deze moeten worden verstrekt door de zorgaanbieders, ziektekostenverzekeraars en degenen, bedoeld in artikel 44.
2. Het eerste lid is mede van toepassing ten aanzien van degene die gegevens verzamelt, bewaart en bewerkt ten behoeve van zorgaanbieders of ziektekostenverzekeraars, alsmede ten aanzien van de groep in de zin van artikel 24b van Boek 2 van het Burgerlijk Wetboek, indien zorgaanbieders of ziektekostenverzekeraars daartoe behoren.
  
*Artikel 63*
  
De in dit hoofdstuk bedoelde gegevens en inlichtingen dienen volledig en naar waarheid te worden verstrekt.
  
*Artikel 65*
  
Onze Minister geeft bij ministeriële regeling aan:
- a. welke van de in artikel 60 onderscheiden categorieën van persoonsgegevens noodzakelijk zijn voor de uitoefening van de in die regeling aangewezen taken en bevoegdheden van de zorgautoriteit;
- b. welke van de in artikel 60 onderscheiden categorieën van persoonsgegevens de zorgautoriteit mag verstrekken aan de in artikel 70 genoemde instanties ten behoeve van de uitoefening van hun taken en bevoegdheden.
  
*Artikel 68*
  
1. De zorgautoriteit kan, met inachtneming van het bepaalde krachtens artikel 65, regels stellen, inhoudende aan wie daarbij te bepalen gegevens en inlichtingen als bedoeld in de artikelen 61 en 62, moeten worden verstrekt, het tijdstip en de wijze waarop en de vorm waarin de gegevens en inlichtingen moeten worden verstrekt of door wie en de wijze waarop de gegevens moeten worden bewerkt of door wie en de wijze waarop de gegevens dan wel de bewerkingen van die gegevens moeten worden bekendgemaakt, alsmede dat een accountant als bedoeld in artikel 393 van Boek 2 van het Burgerlijk Wetboek de juistheid van de verstrekte gegevens en inlichtingen bevestigt.
2. De regels, bedoeld in het eerste lid, kunnen ook inhouden de wijze waarop, de vorm waarin of door wie daarbij te bepalen gegevens en inlichtingen, waaronder medische persoonsgegevens, moeten worden bewerkt alvorens de bewerking moet worden verstrekt.
  
*Artikel 70*
  
1. De zorgautoriteit, het Zorginstituut, het College sanering, de Inspectie gezondheidszorg en jeugd en de Inspectie voor de Sanctietoepassing verstrekken elkaar die gegevens en inlichtingen die van belang kunnen zijn voor de uitoefening van hun wettelijke taken.
2. De zorgautoriteit verstrekt desgevraagd aan de Autoriteit Consument en Markt, de Nederlandsche Bank, de Stichting Autoriteit Financiële Markten, het College bescherming persoonsgegevens en de FIOD-ECD die gegevens en inlichtingen die van belang kunnen zijn voor de uitoefening van hun wettelijke taken.
3. De zorgautoriteit verstrekt desgevraagd aan de Gezondheidsraad, het Rijksinstituut voor de volksgezondheid en milieu, de Raad voor de Volksgezondheid en Zorg, de Raad voor gezondheidsonderzoek, het Centraal Planbureau, het Centraal Bureau voor de Statistiek en het Sociaal Cultureel Planbureau in verband met de beperking van administratieve lasten die gegevens en inlichtingen die van belang kunnen zijn voor de uitoefening van hun wettelijke taken.
4. Bij de verstrekkingen als bedoeld in het eerste tot en met derde lid wordt het bepaalde krachtens artikel 65 in acht genomen.
5. De zorgautoriteit maakt bij de toepassing van het eerste tot en met derde lid geen gebruik van haar bevoegdheden, bedoeld in de artikelen 61 en 64.

<--->

**Toepassing voor de afspraken**

In de *Wet marktordening gezondheidszorg* zijn de rechten van de Nederlandse Zorgautoriteit geregeld m.b.t. het verwerken van persoonsgegevens.
  

*Artikel 60* beschrijft een categorisering van persoonsgegevens, zodat in artikel 65 met lagere regelgeving nadere afspraken hierover kunnen worden gemaakt.
  

*Artikel 61*: NZa heeft recht op alle (persoons)gegevens die nodig zijn voor het uitvoeren van de wettelijke taak.
  

*Artikel 62* stelt dat de NZa regels kan opstellen waarin zij kan vastleggen welke gegevens en inlichtingen regelmatig moeten worden verstrekt door aanbieder en onder welke omstandigheden. Het tweede lid stelt in aanvulling daarop dat de regels ook gelden voor degene die de gegevens verzamelt, bewaart en bewerkt voor de aanbieder.
  

*Artikel 63* waarborgt dat gegevens en inlichtingen door aanbieders volledig en naar waarheid aan de NZa worden verstrekt.
  

*Artikel 65*: In de Regeling categorieën persoonsgegevens WMG is opgenomen welke soorten persoonsgegevens de zorgautoriteit per wettelijke taak mag verwerken voor de uitoefening van de wettelijke taken en mag verstrekken aan de in artikel 70 benoemde (overheids)organisaties.
  

*Artikel 68* geeft de NZa ruimte om regels te stellen over de wijze, het moment en de vorm van gegevensverstrekking door aanbieders. Ook kan zij regels opstellen over wie en op welke wijze gegevens bewerkt, controleert en bekendmaakt.
  

*Artikel 70* is een ruim geformuleerd artikel dat het mogelijk maakt voor de NZa om met andere (overheids)organisaties gegevens uit te wisselen.

{{< /columns >}}