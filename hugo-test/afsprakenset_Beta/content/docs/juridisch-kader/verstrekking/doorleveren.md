---
title: "Doorleveren"
bookToc: false
description: "Doorleveren van informatie"
weight: 110
---

# **{{< param description >}}**


{{< columns >}}

**Relevante artikelen**

Verschillende wetten kennen een artikel waarin de doorlevering van informatie naar (specifieke) partijen is geregeld. Een algemene bepaling op grond waarvan gegevens door een partij doorgeleverd kunnen (of moeten) worden aan andere partijen, is niet gevonden in wet- en regelgeving.
  
<--->

**Toepassing voor de afspraken**

Bij *doorlevering van gegevens* geldt dat van geval tot geval, afhankelijk van welke gegevens het betreft en welke partijen die gegevens hebben of willen hebben, moet uitgezocht worden of deze doorgeleverd mogen worden.
  

Er zijn geen belemmeringen om *informatie door te leveren* die bij het verzamelen openbaar was of na het verzamelen openbaar is geworden. In dat geval zou ieder ander die informatie kunnen verzamelen. 

{{< /columns >}}
