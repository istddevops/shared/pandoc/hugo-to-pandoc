---
title: "Changelog"
description: "De changelog beschrijft de wijzigingen die achtereenvolgens zijn doorgevoerd bij releases van de afsprakenset."
bookCollapseSection: true
weight: 2
---

# Changelog

{{< hint info >}}
{{< param description >}}
{{< /hint >}}