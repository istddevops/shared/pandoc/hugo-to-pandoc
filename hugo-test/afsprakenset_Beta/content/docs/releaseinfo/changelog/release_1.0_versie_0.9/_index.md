---
title: "Release 1.0 versie 0.9"
bookCollapseSection: true
weight: 40
---

# Release 1.0 versie 0.9

De belangrijkste wijzigingen in deze versie zijn:

## Afsprakenset KIK-V
  
- Gewijzigd: De introductie in de afsprakenset is verduidelijkt. Er is een nieuw figuur geschetst en nadere toelichting gegeven op de inhoud van de afsprakenset zijn hierin uitgewerkt. 

## Grondslagen

- Verwijderd: Groeiscenario afsprakenset op de pagina Afbakening eerste conceptversie afsprakenset. Deze is niet meer van toepassing.
- Aangepast: Begrippenlijst is nu opgenomen op de introductiepagina Grondslagen aangezien het een overzicht van begrippen in de grondslagen betreft en geen uitputtende lijst begrippen met betrekking tot de gehele afsprakenset.

## Juridisch kader

- Toegevoegd: toelichting bovenaan pagina over vierdeling ‘aggregatie’, ‘verstrekking’, ‘logging’ en ‘algemeen’.
- Toegevoegd: Artikel 62, 63 en 68 van de Wet Marktordening Gezondheidszorg met grondslagen voor gegevensuitwisseling NZA

## Modelgegevensset

- Gewijzigd: de pagina met toelichting op de modelgegevensset en het gegevenswoordenboek is aangepast. Door uit te gaan van een ontologiemodel voor de modelgegevensset in v0.9 is de structuur van het product modelgegevensset aangepast. Er zijn geen aparte tabbladen voor informatiebehoefte en gegevenswoordenboek. Er is een visueel model van alle concepten en bijbehorende attributen voor in de plaats gekomen.

{{< hint info >}}
Hieronder worden een aantal grotere wijzigingen in de modelgegevensset functioneel toegelicht:

- Verplaatst naar backlog: In v0.9 zijn enkel concepten opgenomen die benodigd zijn voor het beantwoorden van de indicatoren ODB. Hierdoor zijn er een aantal concepten uit de gegevensset verplaatst naar de backlog.
- Gewijzigd: ‘werkovereenkomst’ is het paraplubegrip en alle soorten overeenkomsten vallen onder dit concept.
- Gewijzigd: start- en einddatum zijn niet meer opgenomen als losse elementen, maar zijn dit eigenschappen van bepaalde concepten.
- Gewijzigd: concept zorgverlener gesplitst in twee concepten (concept functie en concept zorgverlener functie).
- Gewijzigd: definitie van gewerkte uren van verloonde uren naar gewerkte / ingezette uren.
- Toegevoegd: concept ziektepercentage.
- Gewijzigd: bijeenkomsten (besprekingen) kunnen als eigenschap datum hebben en een relatie naar een onderwerp.

Een meer uitgebreide toelichting staat omschreven op [Changelog modelgegevensset v0.9](modelgegevensset_v0.9).
{{< /hint >}}

## Model uitwisselprofiel

- Gewijzigd: bij ‘technische interoperabiliteit’ opgenomen dat in een uitwisselprofiel meerdere vormen van technische gegevensuitwisseling mogelijk kunnen zijn voor aanlevering en terugkoppeling. Dit hoeven geen standaardafspraken te zijn. Zo wordt de mogelijkheid voor toepassing portaal of VOLT gecreëerd.
- Toegevoegd aan principes: ‘Voor alle in- en exclusiecriteria die in het uitwisselprofiel worden opgenomen geldt dat zoveel mogelijk wordt aangesloten op in- en exclusiecriteria uit reeds beschikbare uitwisselprofielen'.
- Toegevoegd aan Technische Interoperabiliteit: ‘Afspraken over de werking van de voorzieningen zijn onderdeel van de documentatie van de voorzieningen zelf en worden niet in het uitwisselprofiel beschreven. Het uitwisselprofiel verwijst naar de relevante documentatie’.
- Toegevoegd aan Semantische Interoperabiliteit: ‘Welke contextinformatie meegegeven wordt en vanuit welke bestaande kwalitatieve bron’.
Uitwisselkalender
- Toegevoegd aan doel: Opname van aanlever- en/of peilmoment na vaststelling uitwisselprofiel.

## Beheerafspraken

- Gewijzigd: stappen in het ‘matchingsproces’ zijn enigszins aangepast op basis van ervaringen bij de toepassing van het proces bij informatievragen Patiëntenfederatie en NZA. 
- Gewijzigd: op een aantal plekken bij ‘ontwikkeling uitwisselprofielen’ benadrukt dat de beheerorganisatie een uitwisselprofiel samen met een afnemer ontwikkelt.
- Gewijzigd: bij ‘procedure afwijkende uitvraag’ verduidelijkt dat ook deels kan worden afgeweken van de KIK-V procedures.
- Toegevoegd: sectie ‘ontwikkeling’ waaronder ‘ontwikkeling afsprakenset’, ‘ontwikkeling uitwisselprofielen’ en ‘matchingsproces’ zijn gevoegd.
- Toegevoegd: onderliggende pagina bij ‘ontwikkeling’ een pagina over de wijze waarop zorgaanbieders worden betrokken bij de ontwikkeling.
- Toegevoegd: onderliggende pagina bij ‘matchingsproces’ met ‘bestaande (openbare) bronnen’.
- Toegevoegd: bij ‘ontwikkeling uitwisselprofielen’ dat een uitwisselprofiel minimaal bij een eerste versie en bij grootschalige wijzigingen in samenhang in de praktijk wordt getoetst.
- Toegevoegd: dat de beheerorganisatie een advies opstelt voor het Tactisch overleg en de Ketenraad bij de vaststelling van producten. 
- Toegevoegd: visuele weergave van de samenhang tussen ontwikkeling individuele wijzigingsverzoeken en releases bij ‘ontwikkeling afsprakenset’.
- Toegevoegd: dat de beheerorganisatie, waar nodig en gepast, voorzieningen kan aanbieden ten behoeve van de operationele gegevensuitwisseling.

## Sturingsstructuur

- Verwijderd: argumenten voor de keuze van ZIN als beheerorganisatie.

{{< hint info >}}
Met de publicatie van deze versie van de afsprakenset zijn ook wijzigingen doorgevoerd in de volgende producten die los van de afsprakenset worden gepubliceerd:

- Uitwisselprofiel ZIN - ODB
- Gewijzigd: Op de semantische laag zijn de gegevens toegepast uit de modelgegevensset v0.9 (obv ontologie) en berekeningen op basis van die gegevens
Convenant KIK-V
- Wijzigingen van 0.8 naar 0.85 (obv expertview VWS)
  - In het artikel over de beheerorganisatie (artikel 5) is de laatste bullet verplaatst naar een eerste bullet om aan te geven dat er een beheerorganisatie zal zijn. Tevens is de tekst daarvan wat verduidelijkt. Daarna volgen de artikelen die gaan over de activiteiten.
  - Onder artikel 16 (over Geschillen) zijn elementen uit artikel 18 opgenomen die gingen over geschilbeslechting (punten 4 en 5). Daarmee heeft het een logischere plek gekregen.
  - Er is een tabel toegevoegd voor de daadwerkelijke ondertekening van het convenant.
  - Na ondertekening van het convenant wordt deze gepubliceerd in de Staatscourant.
- Wijzigingen van 0.85 naar 0.9 (obv voorbereiding bestuurlijke ronde)
  - NZA: Artikel 16 lid 4: “Stichting Nederlands MediationInstituut” (NMI) lijkt al een tijdje niet meer te bestaan, en is voor zover ons bekend opgegaan in de Mediators federatie Nederland (MfN). → Aangepast.
- Wijzigingen van 0.9 naar 0.92 (obv input voorafgaand aan vaststelling door Ketenraad 1 dec)
  - Tabel voor ondertekening worden aparte ondertekenvellen per organisatie
  - VGZ: Artikel 17 suggereerde dat beëindiging van deelname van een van de partijen drie maanden voor aflopen van het convenant tot beëindiging van het convenant leidt. → Dit is niet de bedoeling en daarom aangepast. Het convenant eindigt slechts als partijen drie maanden voor aflopen van het convenant gezamenlijk besluiten het convenant te beëindigen. Dit laat onverlet dat (volgend uit artikel 11) een partij deelname aan het convenant kan opzeggen met inachtneming van een opzegtermijn van drie maanden.
  - IGJ: Het is de bedoeling om het convenant te publiceren in de Staatscourant. In het convenant is dit echter niet terug te vinden. → Artikel 19 toegevoegd over publicatie in de Staatscourant.
  - IGJ: Het is niet handig om de lijst met (beleids)medewerkers van het Tactisch Overleg als bijlage bij het convenant te voegen. Dan moet bij iedere wijziging in de samenstelling (de bijlage bij) het convenant opnieuw worden vastgesteld. → Aangepast. Nu is het een verantwoordelijkheid van de beheerorganisatie om de lijst met deelnemers bij te houden. 
- Toegevoegd: intentieverklaring andere belanghebbende partijen

{{< /hint >}}