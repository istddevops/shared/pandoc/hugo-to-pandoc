#!/usr/bin/env node
"use strict"

/**
 * Node Package Modules
 */
import { argv } from 'process';

/**
 * Local Library Modules
 */
import { convertHugoBookContentToPanDocMd } from  '../lib';

//
// START CLI Script
//
convertHugoBookContentToPanDocMd(argv[2], argv[3], argv[4]);
