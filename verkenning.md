# Verkenning HUGO SSG naar PanDoc document generatie

Verkenning om te komen tot volledig geautomatiseerde verwerking van HUGO SSG (inclusief Book Theme en KIK-V template maatwerk) Markdoen content en configuratie naar meerdere generereerde document uitvoer opties via R Mardown input en LaTeX engine verwerking.

Huidige status:

- [X] Basis LaTeX PDF generatie 
- [ ] Sub Chapter Leveling by adding `#` to Markdow-Headings
- [ ] KROKI.IO rendering using https://gitlab.com/myriacore/pandoc-kroki-filter
 or TypeScript-export?
- [ ] As alternative Build conversion from HUGO ShortCodes to R Markdoen using PanDoc TypeScript Filters. See https://pandoc.org/filters.html and https://github.com/mathematic-inc/node-pandoc-filter

Generate documents from Markdown with the PanDoc Tool

## Install PanDoc MacOs

See https://pandoc.org/installing.html#macos

## Used references

- https://pandoc.org/MANUAL.html#creating-a-pdf
- https://pandoc.org/MANUAL.html#variables-for-latex
- [R Markdown Cookbook](https://bookdown.org/yihui/rmarkdown-cookbook)
- https://www.overleaf.com/learn
- https://xdev.andreasherten.de/2016/01/26/latex-beamer-with-markdown.html
- https://texfaq.org/FAQ-optionclash

## HUGO Book ShortCode hint

Using LaTeX `tcolorbox` package.  
Based on:
- https://learnbyexample.github.io/customizing-pandoc/#stylish-blockquote
- https://www.overleaf.com/learn/latex/Environments#Defining_environments_with_parameters

LateX package requirements:

- `tlmgr install tcolorbox`  
- `tlmgr install environ` (to prevent error **! LaTeX Error: File `environ.sty' not found.**)

Additions to `template.tex`

```tex
% {{< hint <%= parm1 %> >}}
\usepackage{tcolorbox}
\newtcolorbox{hint}[1]{colback=#1!5!white, colframe=#1!75!black}
\renewenvironment{quote}{\begin{hint}}{\end{hint}}
% {{< / hint >}}
```

### Generic Solution to Parse Markdown inside LatEx Block

Based on: https://github.com/jgm/pandoc/issues/2453#issuecomment-219233316

Additions to `template.tex`

```tex
% inside Markdown parsing - begin
\newcommand{\hideFromPandoc}[1]{#1}
  \hideFromPandoc{
    \let\Begin\begin
    \let\End\end
  }
% inside Mardown parsing - end
```

## Example in LateX code

```md
\Begin{hint}{red}
Het moet een overzicht geven voor bestuurders, directeuren IV/IT, CxO’s en architecten van de betrokken ketenpartijen (zoals ZIN, IGJ, Nza, Verpleeghuizen, ...) die affiniteit hebben met de digitalisering van zorgketens. Het document bevat diverse verwijzingen naar andere **producten** die in het kader van het programma zijn opgeleverd. Deze documenten bieden aanvullende achtergronden en detaillering.
\End{hint}
```

## Defaults File

See https://pandoc.org/MANUAL.html#option--defaults

See https://pandoc.org/MANUAL.html#default-files
